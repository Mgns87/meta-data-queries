/**
 * Module dependencies.
 */

var express = require('express'),
    http = require('http'),
    path = require('path'),
    routes = require('./routes');

var app = express();

// All environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.engine('html', require('ejs')
    .renderFile);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public/ontov')));


//Frontend Angular routes
app.use(routes.index); //Catch all routes that did not match other middleware paths

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

http.createServer(app)
    .listen(app.get('port'), function() {
        console.log('Express server listening on port ' + app.get('port'));
    });