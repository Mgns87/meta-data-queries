angular.module('ontov')
    .directive('ontovViz', ['d3Service', '$window', 'qeService', 'dataModel', function(d3Service, $window, qeService, dataModel) {
        return {
            restrict: 'E',
            scope: {
                data: '@input',
                width: '@width',
                height: '@height'
            },
            templateUrl: 'template/visualization.html',
            link: function(scope, element, attrs) {

                //Init View Model
                var vm = {};
                var svg = element.find('svg')[0];
                svg.style.height = '1000px';
                svg.style.width = '100%';
                svg.style.overflow = 'scroll';

                //Init D3
                d3Service.get()
                    .then(function(d3) {
                        this.d3 = d3;
                    });
                scope.$on('dataModel::ready', function(event){

                    vm = dataModel.getViewModel();
                    scope.nodes = vm.nodes,
                        scope.links = vm.links;
                    drawD3Tree();
                });

                //Listen to data model changes
                scope.$on('dataModel::viewModelUpdate', function(event) {
                    vm = dataModel.getViewModel();
                    scope.nodes = vm.nodes,
                        scope.links = vm.links;
                    drawD3Tree();
                    scope.$apply();
                });

                //Update visualization
                function drawD3Tree() {
                    if(!scope.nodes || scope.nodes.length === 0){
                        return;
                    }

                    var d3 = this.d3;
                    if (d3) {
                        var tree = d3.layout.tree();
                        var clientRect = svg.getClientRects()[0];
                         tree.size([1000, clientRect.width-200]);
                            tree.separation(function separation(a, b) {
                                return a.parent == b.parent ? 50 : 50;
                            });
                            tree.nodes(scope.nodes[0]);
                    }
                }
            }
        }
    }]);
