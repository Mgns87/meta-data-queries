//http://www.ng-newsletter.com/posts/d3-on-angular.html
(function() {
    angular.module('ontov')
        .service('d3Service', ['$document', '$q', '$rootScope', //D3 js
            function($document, $q, $rootScope) {
                var d = $q.defer();

                function onScriptLoad() {
                    // Load client in the browser
                    $rootScope.$apply(function() {
                        d.resolve(window.d3);
                    });
                    console.log("d3Service::D3 ready");
                }
                // Create a script tag with d3 as the source
                // and call our onScriptLoad callback when it
                // has been loaded
                var scriptTag = $document[0].createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.async = true;
                scriptTag.src = 'js/vendor/d3.min.js';
                scriptTag.charset = 'utf-8';
                scriptTag.onreadystatechange = function() {
                    if (this.readyState == 'complete') onScriptLoad();
                };
                
                scriptTag.onload = onScriptLoad;
                var s = $document[0].getElementsByTagName('body')[0];
                s.appendChild(scriptTag);
                return {
                    get: function() {

                        return d.promise;
                    }
                };
            }
        ])
        .service('vsService', ['$document', '$q', '$rootScope', //Visualsearch js
            function($document, $q, $rootScope) {
                var d = $q.defer();

                var scriptTag = $document[0].createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.async = true;
                scriptTag.src = 'js/vendor/dependencies.js';
                scriptTag.charset = 'utf-8';
                scriptTag.onreadystatechange = function() {
                    if (this.readyState == 'complete') onDependencyLoad();
                }
                scriptTag.onload = onDependencyLoad;
                var s = $document[0].getElementsByTagName('body')[0];
                s.appendChild(scriptTag);

                function onDependencyLoad() {
                    // Create a script tag with visual search as the source
                    // and call our onScriptLoad callback when it
                    // has been loaded
                    var scriptTag = $document[0].createElement('script');
                    scriptTag.type = 'text/javascript';
                    scriptTag.async = true;
                    scriptTag.src = 'js/vendor/visualsearch.js';
                    scriptTag.charset = 'utf-8';

                    scriptTag.onreadystatechange = function() {
                        if (this.readyState == 'complete') onScriptLoad();
                    }
                    scriptTag.onload = onScriptLoad;
                    var s = $document[0].getElementsByTagName('body')[0];
                    s.appendChild(scriptTag);

                    function onScriptLoad() {
                        $rootScope.$apply(function() {
                            d.resolve(window.VS);
                            console.log("vsService::Visualsearch ready");
                        });
                    }
                }

                return {
                    get: function() {

                        return d.promise;
                    }
                };
            }
        ])
        .service('qeService', ['$document', '$q', '$rootScope', 'bbService', //Queryengine js
            function($document, $q, $rootScope, bbService) {

                var d = $q.defer();
                bbService.get() //Get Backbone
                    .then(function() {
                        function onScriptLoad() {
                            // Load client in the browser
                            $rootScope.$apply(function() {
                                d.resolve(window.queryEngine);
                                console.log("qeService::Queryengine ready");
                            });
                        }
                        // Create a script tag with queryEngine as the source
                        // and call our onScriptLoad callback when it
                        // has been loaded
                        var scriptTag = $document[0].createElement('script');
                        scriptTag.type = 'text/javascript';
                        scriptTag.async = true;
                        scriptTag.src = 'js/vendor/query-engine.js';
                        scriptTag.charset = 'utf-8';
                        scriptTag.onreadystatechange = function() {
                            if (this.readyState == 'complete') onScriptLoad();
                        }
                        scriptTag.onload = onScriptLoad;
                        var s = $document[0].getElementsByTagName('body')[0];
                        s.appendChild(scriptTag);
                    })

                return {
                    get: function() {
                        return d.promise;
                    }
                };
            }
        ])
        .service('bbService', ['$document', '$q', '$rootScope', 'usService', //Backbone js
            function($document, $q, $rootScope, usService) {
                var d = $q.defer();

                usService.get() //Underscore js dependency
                    .then(function(underScore) {
                        function onScriptLoad() {
                            // Load client in the browser
                            $rootScope.$apply(function() {
                                d.resolve(window.Backbone);
                                console.log("bbService::Backbone ready");
                            });
                        }
                        // Create a script tag with Backbone as the source
                        // and call our onScriptLoad callback when it
                        // has been loaded
                        var scriptTag = $document[0].createElement('script');
                        scriptTag.type = 'text/javascript';
                        scriptTag.src = 'js/vendor/backbone-1.1.2.min.js';
                        scriptTag.charset = 'utf-8';
                        scriptTag.onreadystatechange = function() {
                            if (this.readyState == 'complete') onScriptLoad();
                        }
                        scriptTag.onload = onScriptLoad;
                        var s = $document[0].getElementsByTagName('body')[0];
                        s.appendChild(scriptTag);

                    });
                return {
                    get: function() {
                        return d.promise;
                    }
                };
            }
        ])
        .service('usService', ['$document', '$q', '$rootScope', //Underscore js
            function($document, $q, $rootScope) {
                var d = $q.defer();

                function onScriptLoad() {
                    // Load client in the browser
                    $rootScope.$apply(function() {
                        d.resolve(window.Underscore);
                        console.log("usService::Underscore ready");
                    });
                }
                // Create a script tag with Underscore as the source
                // and call our onScriptLoad callback when it
                // has been loaded
                var scriptTag = $document[0].createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.src = 'js/vendor/underscore-1.8.3.min.js';
                scriptTag.charset = 'utf-8';
                scriptTag.onreadystatechange = function() {
                    if (this.readyState == 'complete') onScriptLoad();
                }
                scriptTag.onload = onScriptLoad;
                var s = $document[0].getElementsByTagName('body')[0];
                s.appendChild(scriptTag);
                return {
                    get: function() {
                        return d.promise;
                    }
                };
            }
        ]);
        // .config(['CollaboPluginServiceProvider', function (CPServiceProvider){ //KnAllEdge plugin config
        //      //Ontov KnAllEdge specific options
        //         var ontovPluginOptions = {
        //             name: "ontovPluginOptions",
        //             private: {},
        //             references: {
        //                 map: {
        //                     items: {
        //                         config: null,
        //                         mapStructure: null
        //                     },
        //                     resolved: false,
        //                     callback: null
        //                 }
        //             },
        //             components: {},
        //             events: {}
        //         };

        //     CPServiceProvider.registerPlugin(ontovPluginOptions);
        // }])
})();