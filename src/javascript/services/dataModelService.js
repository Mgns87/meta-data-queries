(function() {
    'use strict';

    angular.module('ontov')
        .service('dataModel', ['$rootScope', '$timeout', 'qeService', 'usService', '$q', '$http', function($rootScope, $timeout, qeService, usService, $q, $http) {
                //http://joelhooks.com/blog/2013/04/24/modeling-data-and-state-in-your-angularjs-application/

                var performSearch = function() {},
                    projectCollection = {},
                    projectSearchCollection = {};

                var viewModel = {
                        "nodeTypes": {
                            "queryNodes": [],
                            "pathNodes": []
                        },
                        "nodes": [],
                        "links": []
                    },
                    knalledgeMap = {};

                //https://mgechev.github.io/javascript-algorithms/graphs_others_topological-sort.js.html
                var topologicalSort = (function() {
                    function topologicalSortHelper(node, visited, temp, graph, result) {
                        temp[node] = true;
                        var neighbors = [],
                            nodeIndex = 0,
                            nodeInstance = {};
                        //Get current no
                        for (var i = graph.length - 1; i >= 0; i--) {
                            if (graph[i]._id === node) {
                                nodeInstance = graph[i];
                                neighbors = graph[i].getIds('children');
                                nodeIndex = i;
                            }
                        };

                        for (var i = 0; i < neighbors.length; i += 1) {
                            var n = neighbors[i];
                            if (temp[n]) {
                                throw new Error('The graph is not a DAG');
                            }
                            if (!visited[n]) {
                                topologicalSortHelper(n, visited, temp, graph, result);
                            }
                        }
                        temp[node] = false;
                        visited[node] = true;
                        result.push(nodeInstance);
                    }
                    /**
                     * Topological sort algorithm of a directed acyclic graph.<br><br>
                     * Time complexity: O(|E|) where E is a number of edges.
                     */
                    return function(graph) {
                        var result = [];
                        var visited = [];
                        var temp = [];
                        for (var node in graph) {
                            if (!visited[graph[node]._id] && !temp[graph[node]._id]) {
                                topologicalSortHelper(graph[node]._id, visited, temp, graph, result);
                            }
                        }
                        return result.reverse();
                    };
                }());

                //Find shortest path in topological ordered DAG(Directed Acyclic graph)
                var shortPathDAG = (function() {
                    var cost = {}; // A mapping between a node, the cost of it's shortest path, and it's parent in the shortest path
                    function findPath(topSortGraph, sourceNode, destNode) {
                        //http://stackoverflow.com/questions/1482619/shortest-path-for-a-dag

                        // //Find the graph index of the source and destination nodes
                        // var startIndex = 0, endIndex = 0, temp = 0;
                        // for (var i = 0; i < topSortGraph.length; i++) {
                        //     if(sourceNode._id === topSortGraph[i]._id)
                        //         startIndex = i;
                        //     else if (destNode._id === topSortGraph[i]._id)
                        //         endIndex = i;
                        // }
                        //If the destination somehow has a lower index that source. Reverse their values.
                        // if(startIndex<endIndex){
                        //         temp = startIndex;
                        //         startIndex = endIndex;
                        //         endIndex = startIndex;
                        //     }

                        // for each vertex v in top_sorted_list:
                        for (var i = 0; i < topSortGraph.length; i++) {
                            //   cost[vertex].cost = inf
                            cost[topSortGraph[i]._id] = {};
                            cost[topSortGraph[i]._id].cost = Infinity;
                            //   cost[vertex].parent = None
                            cost[topSortGraph[i]._id].parent = null;
                        }
                        cost[sourceNode._id].cost = 0;
                        for (var u = 0; u < topSortGraph.length; u++) {
                            //for each vertex v in top_sorted_list:
                            var edges = topSortGraph[u].childrenLinks;
                            //for each edge e in adjacenies of v:
                            for (var y = 0; y < edges.length; y++) {
                                //if cost[e.dest].cost < cost[v].cost + e.weight:
                                var weight = edges[y].weight ? edges[y].weight : 1; //If edges do not have weights. Set value to 1.
                                if (cost[edges[y].targetId].cost > cost[topSortGraph[u]._id].cost + weight) {
                                    //cost[e.dest].cost =  cost[v].cost + e.weight
                                    cost[edges[y].targetId].cost = cost[topSortGraph[u]._id].cost + weight;
                                    //cost[e.dest].parent = v

                                    cost[edges[y].targetId].parent = topSortGraph[u];
                                }
                            }
                        }
                    }

                    return function(topSortGraph, sourceNode, destNode) {
                        if (!topSortGraph || !sourceNode || !destNode) {
                            return console.log('dataModel::shortPathDAG::"Arguments missing. Aborting.');
                        }else if (sourceNode._id === destNode._id){
                            return console.log("dataModelService::shortPathDAG::Source and destination node is the same. Aborting.");
                        }

                        findPath(topSortGraph, sourceNode, destNode);
                        var subgraph = [];
                        var target = cost[destNode._id];
                        var temp = target;
                        subgraph.push(destNode);
                        //NB. Only checks single parent nodes
                        while (temp.parent && temp._id != sourceNode._id) { //Backtrack target parent chain to find shortest path to source
                            subgraph.push(temp.parent);
                            temp = cost[temp.parent._id];
                            //If source cannot be found. Source is likely on another tree branch.
                            if (!sourceNode) {
                                console.log("dataModel::shortPathDAG:: No source node, source set to root.");
                            }
                        }
                        return subgraph.reverse();
                    };
                }());

                $q.all([$http.get('data/2016.07.18-TNC-Online-kedges.json')
                        .then(function(response) {
                            knalledgeMap.kedges = response.data;
                        }),
                        $http.get('data/2016.07.18-TNC-Online-kmaps.json')
                        .then(function(response) {
                            knalledgeMap.kmaps = response.data;
                        }),
                        $http.get('data/2016.07.18-TNC-Online-knodes.json')
                        .then(function(response) {
                            knalledgeMap.knodes = response.data;
                        })
                    ])
                    .then(function() {
                        console.log("dataModel::KnAllEdge map data received");
                        //Create children and parrent arrays on all nodes

                        buildInheritanceTree(knalledgeMap.knodes, knalledgeMap.kedges);
                        //Perform a topological sort
                        var topSort = topologicalSort(knalledgeMap.knodes);
                        viewModel = setViewModel(topSort, []);

                        //Fetch Query Engine
                        qeService.get()
                            .then(function(queryEngine) {

                                //Create our project collection from an array of models
                                projectCollection = queryEngine.createLiveCollection(topSort);

                                projectSearchCollection = projectCollection.createLiveChildCollection()
                                    .setPill('Name', {
                                        prefixes: ['name:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('name') === currentValue);
                                            })

                                            return result;

                                        }
                                    })
                                    .setPill('Type', {
                                        prefixes: ['Type:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('type') === currentValue);
                                            })

                                            return result;
                                        }
                                    })
                                    .setPill('Question', {
                                        prefixes: ['Question:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_ibis_question') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }
                                            else return false;
                                        }
                                    })
                                    .setPill('Knowledge', {
                                        prefixes: ['Knowledge:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_knowledge') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }

                                            else return false;
                                        }
                                    })
                                    .setPill('Idea', {
                                        prefixes: ['Idea:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_ibis_idea') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }

                                            else return false;
                                        }
                                    })
                                    .setPill('Argument', {
                                        prefixes: ['Argument:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_ibis_argument') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }
                                            else return false;
                                        }
                                    })
                                    .setPill('Author', {
                                        prefixes: ['Author:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('iAmId') === currentValue);
                                            })

                                            return result;
                                        }
                                    })
                                    .setPill('Created', {
                                        prefixes: ['Created:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('createdAt') === currentValue);
                                            })

                                            return result;
                                        }
                                    })
                                    .setPill('Updated', {
                                        prefixes: ['Updated:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('updatedAt') === currentValue);
                                            })

                                            return result;
                                        }
                                    })

                                .setFilter('search', function(model, searchString) {
                                    if (!searchString) {
                                        return true;
                                    } else {
                                        var searchRegex = queryEngine.createSafeRegex(searchString);
                                        var passName = searchRegex.test(model.get("name"));
                                        var dataContent = model.get("dataContent");
                                        var passDescription = false;
                                        if(dataContent && dataContent.property){
                                            passDescription = searchRegex.test(dataContent.properproperty)
                                        }

                                        return passName || passDescription;
                                    }
                                });

                                var nodes = [];
                                var startIndex = 0;
                                //Done
                                dataModelReady();

                            });

                    }, function(reason) {
                        console.log("dataModel::KnAllEdge map data error");
                    });

                function splitOR(value) {
                    var properties = [];

                    if(value) {
                        properties = value.split(" or ");
                    }
                    return properties;
                }

                function splitAND(value) {
                    var properties = [];
                    if(value) {
                        properties = value.split(" and ");
                    }
                    return properties;
                }

                function buildInheritanceTree(nodes, edges) {
                    var _nodes = Array.prototype.concat(nodes, []); //Create a copy
                    for (var y = _nodes.length - 1; y >= 0; y--) {
                        _nodes[y].parents = [],
                            _nodes[y].children = [],
                            _nodes[y].parentsLinks = [],
                            _nodes[y].childrenLinks = [];
                        /* Get list property ids
                         * @param property : Name of object property to traverse
                         * @return : List of ids
                         */

                        _nodes[y].getIds = function(property) {
                            var _array = [],
                                _collection = Array.prototype.concat(this[property]);
                            for (var index in _collection) {
                                _array.push(_collection[index]._id);
                            }
                            return _array;
                        }
                    }

                    for (var i = edges.length - 1; i >= 0; i--) {
                        var sourceId = edges[i].sourceId,
                            targetId = edges[i].targetId;

                        var sourceNode = _nodes.filter(function(n) {
                                return n._id === sourceId;
                            })[0],
                            targetNode = _nodes.filter(function(n) {
                                return n._id === targetId;
                            })[0];
                        //Parent(s) are always the source of a link
                        //Child(ren) are always the target of a link
                        //Neighbors are both parents and
                        if (targetNode && sourceNode) {
                            targetNode.parents.push(sourceNode);
                            targetNode.parentsLinks.push(edges[i]);
                            sourceNode.children.push(targetNode);
                            sourceNode.childrenLinks.push(edges[i]);
                        }

                    }
                    return _nodes;
                }

                //Perform search on collection - return querynodes
                function searchCollection(searchString) {
                    var queries = searchString.split(",");
                    var queryNodes = [];
                    _.each(queries, function(searchTerm, index, list) {
                        if (/\S/.test(searchTerm) && searchTerm !== null) { //Check that search string contains at least 1 character
                            var queryNode = projectSearchCollection
                                .setSearchString(searchTerm)
                                .query()
                                .toJSON();
                            queryNodes.push({
                                "type": "queryNode",
                                "queryString": searchTerm,
                                "label": searchTerm,
                                nodes: queryNode
                            });
                        }
                    });

                    return queryNodes;
                }

                //Search for paths between query nodes - return pathnodes
                function searchQueryPaths(queryCollection, queryNodes) {
                    if (!queryNodes || queryNodes.length <= 0)
                        return [];
                    var _paths = [],
                        _startIndex = 0;
                    console.log(queryNodes);
                    //TODO: Cross check all queries. Currently only works with one result per query node.
                    //Update: Should work fine now
                    for (var i = 0; i < queryNodes.length; i++) {
                        for (var y = 0; y < queryNodes[i].nodes.length; y++) {
                            var node = queryNodes[i].nodes[y];
                            var _path = findPaths(queryCollection, queryCollection[0], node);
                            _paths.push(_path);
                        }

                    }
                    console.log("Paths found:");
                    console.log(_paths);
                    //Find all paths between two nodes
                    function findPaths(topSortGraph, qnOne, qnTwo) {
                        return shortPathDAG(topSortGraph, qnOne, qnTwo);
                    }

                    return _paths;
                }
                //Merges all paths into one graph.
                function mergePaths(paths) {
                    var _paths = Array.prototype.concat(paths, []);
                    var _graph = {},
                        _objGraph = [];

                    while (_paths.length > 0) {
                        var temp = _paths.pop();
                        for (var y = 0; y < temp.length; y++) {
                            _graph[temp[y]._id] = temp[y];
                        }
                    }
                    for (var property in _graph) {
                        _objGraph.push(_graph[property]);
                    }
                    return _objGraph;
                }

                //Top down sort nodes according to given property - returns sorted array
                function sortNodesPath(nodesArray, property) {
                    if (!nodesArray || !nodesArray.length > 0)
                        return [];
                    var sortedArray = _.sortBy(nodesArray, function(item, index, list) {
                        if (item[property])
                            return -item[property].length;
                        else return 0;
                    });
                    return sortedArray;
                }

                //Set result node size relative to paths it contain and other result nodes
                //Assumes top down sorted array - returns resized nodes array
                function setNodesSize(sortedNodeArray) {
                    if (!sortedNodeArray || !sortedNodeArray.length > 0)
                        return [];
                    var resizedNodes = sortedNodeArray;
                    //Largest possible size
                    var _maxSize = 1;
                    //Total paths on biggest result node
                    var _topNode = null;

                    _.each(resizedNodes, function(item, index, list) {
                        if (!_topNode) {
                            item.size = _maxSize;
                            _topNode = item;
                        } else {
                            item.size = item.paths.length / _topNode.paths.length;
                        }
                    });

                    return resizedNodes;
                }

                //Converge query nodes, path nodes and links and update the view model - returns view model
                function setViewModel(nodes) {
                    viewModel.nodes = nodes;
                    var links = [];

                    knalledgeMap.kedges.forEach(function(e) {
                        //http://stackoverflow.com/questions/16824308/d3-using-node-attribute-for-links-instead-of-index-in-array
                        // Get the source and target nodes
                        var sourceNode = viewModel.nodes.filter(function(n) {
                                return n._id === e.sourceId;
                            })[0],
                            targetNode = viewModel.nodes.filter(function(n) {
                                return n._id === e.targetId;
                            })[0];

                        // Add the edge to the array
                        if (sourceNode && targetNode)
                            links.push({
                                source: sourceNode,
                                target: targetNode
                            });
                    });

                    viewModel.links = links;

                    return viewModel;
                }

                //Signal that viewmodel is ready
                function dataModelReady() {
                    return $rootScope.$broadcast('dataModel::ready');
                }

                //Broadcast viewmodel updates
                function broadcastUpdate() {
                    return $rootScope.$broadcast('dataModel::viewModelUpdate');
                }

                return {
                    search: function(searchString) {
                        // console.groupCollapsed("dataModel::Search");
                        //Update viewmodel with search results
                        var _queryNodes = searchCollection(searchString);
                        var _topSort = topologicalSort(projectCollection.toJSON());
                        var _pathNodes = searchQueryPaths(_topSort, _queryNodes);
                        var _graph = mergePaths(_pathNodes);

                        // var nodes = [];
                        // for (var index in _pathNodes) {
                        //     nodes = Array.prototype.concat(_pathNodes[index].path, nodes);
                        // }

                        // var _sortedPathNodes = sortNodesPath(_pathNodes, 'paths');
                        // var _resizedNodes = setNodesSize(_sortedPathNodes);

                        // console.log("dataModel.search:: Found %i query nodes and %i path nodes.", _queryNodes.length, _pathNodes.length);

                        //Set dataModel.viewModel
                        _graph = buildInheritanceTree(_graph, knalledgeMap.kedges);
                        console.log("Graph print: ");
                        console.log(_graph);
                        setViewModel(_graph);

                        //console.log(viewModel.nodes);
                        //console.log(viewModel.links);

                        // //Tell all views that viewmodel is updated
                        broadcastUpdate();

                        // console.groupEnd();
                    },
                    getPills: function() {
                        return projectSearchCollection.getPills();
                    },
                    getFacetMatches: function(facet) {
                        var _val = '';
                        switch (facet) {
                            case 'Question':
                                _val = 'type_ibis_question';
                                break;
                            case 'Idea':
                                _val = 'type_ibis_idea';
                                break;
                            case 'Argument':
                                _val = 'type_ibis_argument';
                                break;
                            case 'Knowledge':
                                _val = 'type_knowledge';
                                break;
                              case 'Name':
                               return _.compact(_.uniq(_.flatten(projectCollection.pluck('name'))));
                                break;
                            case 'Author':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('iAmId'))));
                                break;
                            case 'Type':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('type'))));
                                break;
                            case 'Created':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('createdAt'))));
                                break;
                            case 'Updated':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('updatedAt'))));
                                break;
                            default:
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck(facet))));
                        }
                        return _.invoke(projectCollection.where({
                            type: _val
                        }), 'get', 'name');
                    },
                    getViewModel: function() {
                        return viewModel;
                    }

                };

            }

        ]);
})();
