//Hello World
angular.module('ontov', []);
(function() {
    'use strict';

    angular.module('ontov')
        .service('dataModel', ['$rootScope', '$timeout', 'qeService', 'usService', '$q', '$http', function($rootScope, $timeout, qeService, usService, $q, $http) {
                //http://joelhooks.com/blog/2013/04/24/modeling-data-and-state-in-your-angularjs-application/

                var performSearch = function() {},
                    projectCollection = {},
                    projectSearchCollection = {};

                var viewModel = {
                        "nodeTypes": {
                            "queryNodes": [],
                            "pathNodes": []
                        },
                        "nodes": [],
                        "links": []
                    },
                    knalledgeMap = {};

                //https://mgechev.github.io/javascript-algorithms/graphs_others_topological-sort.js.html
                var topologicalSort = (function() {
                    function topologicalSortHelper(node, visited, temp, graph, result) {
                        temp[node] = true;
                        var neighbors = [],
                            nodeIndex = 0,
                            nodeInstance = {};
                        //Get current no
                        for (var i = graph.length - 1; i >= 0; i--) {
                            if (graph[i]._id === node) {
                                nodeInstance = graph[i];
                                neighbors = graph[i].getIds('children');
                                nodeIndex = i;
                            }
                        };

                        for (var i = 0; i < neighbors.length; i += 1) {
                            var n = neighbors[i];
                            if (temp[n]) {
                                throw new Error('The graph is not a DAG');
                            }
                            if (!visited[n]) {
                                topologicalSortHelper(n, visited, temp, graph, result);
                            }
                        }
                        temp[node] = false;
                        visited[node] = true;
                        result.push(nodeInstance);
                    }
                    /**
                     * Topological sort algorithm of a directed acyclic graph.<br><br>
                     * Time complexity: O(|E|) where E is a number of edges.
                     */
                    return function(graph) {
                        var result = [];
                        var visited = [];
                        var temp = [];
                        for (var node in graph) {
                            if (!visited[graph[node]._id] && !temp[graph[node]._id]) {
                                topologicalSortHelper(graph[node]._id, visited, temp, graph, result);
                            }
                        }
                        return result.reverse();
                    };
                }());

                //Find shortest path in topological ordered DAG(Directed Acyclic graph)
                var shortPathDAG = (function() {
                    var cost = {}; // A mapping between a node, the cost of it's shortest path, and it's parent in the shortest path
                    function findPath(topSortGraph, sourceNode, destNode) {
                        //http://stackoverflow.com/questions/1482619/shortest-path-for-a-dag

                        // //Find the graph index of the source and destination nodes
                        // var startIndex = 0, endIndex = 0, temp = 0;
                        // for (var i = 0; i < topSortGraph.length; i++) {
                        //     if(sourceNode._id === topSortGraph[i]._id)
                        //         startIndex = i;
                        //     else if (destNode._id === topSortGraph[i]._id)
                        //         endIndex = i;
                        // }
                        //If the destination somehow has a lower index that source. Reverse their values.
                        // if(startIndex<endIndex){
                        //         temp = startIndex;
                        //         startIndex = endIndex;
                        //         endIndex = startIndex;
                        //     }

                        // for each vertex v in top_sorted_list:
                        for (var i = 0; i < topSortGraph.length; i++) {
                            //   cost[vertex].cost = inf
                            cost[topSortGraph[i]._id] = {};
                            cost[topSortGraph[i]._id].cost = Infinity;
                            //   cost[vertex].parent = None
                            cost[topSortGraph[i]._id].parent = null;
                        }
                        cost[sourceNode._id].cost = 0;
                        for (var u = 0; u < topSortGraph.length; u++) {
                            //for each vertex v in top_sorted_list:
                            var edges = topSortGraph[u].childrenLinks;
                            //for each edge e in adjacenies of v:
                            for (var y = 0; y < edges.length; y++) {
                                //if cost[e.dest].cost < cost[v].cost + e.weight:
                                var weight = edges[y].weight ? edges[y].weight : 1; //If edges do not have weights. Set value to 1.
                                if (cost[edges[y].targetId].cost > cost[topSortGraph[u]._id].cost + weight) {
                                    //cost[e.dest].cost =  cost[v].cost + e.weight
                                    cost[edges[y].targetId].cost = cost[topSortGraph[u]._id].cost + weight;
                                    //cost[e.dest].parent = v

                                    cost[edges[y].targetId].parent = topSortGraph[u];
                                }
                            }
                        }
                    }

                    return function(topSortGraph, sourceNode, destNode) {
                        if (!topSortGraph || !sourceNode || !destNode) {
                            return console.log('dataModel::shortPathDAG::"Arguments missing. Aborting.');
                        }else if (sourceNode._id === destNode._id){
                            return console.log("dataModelService::shortPathDAG::Source and destination node is the same. Aborting.");
                        }

                        findPath(topSortGraph, sourceNode, destNode);
                        var subgraph = [];
                        var target = cost[destNode._id];
                        var temp = target;
                        subgraph.push(destNode);
                        //NB. Only checks single parent nodes
                        while (temp.parent && temp._id != sourceNode._id) { //Backtrack target parent chain to find shortest path to source
                            subgraph.push(temp.parent);
                            temp = cost[temp.parent._id];
                            //If source cannot be found. Source is likely on another tree branch.
                            if (!sourceNode) {
                                console.log("dataModel::shortPathDAG:: No source node, source set to root.");
                            }
                        }
                        return subgraph.reverse();
                    };
                }());

                $q.all([$http.get('data/2016.07.18-TNC-Online-kedges.json')
                        .then(function(response) {
                            knalledgeMap.kedges = response.data;
                        }),
                        $http.get('data/2016.07.18-TNC-Online-kmaps.json')
                        .then(function(response) {
                            knalledgeMap.kmaps = response.data;
                        }),
                        $http.get('data/2016.07.18-TNC-Online-knodes.json')
                        .then(function(response) {
                            knalledgeMap.knodes = response.data;
                        })
                    ])
                    .then(function() {
                        console.log("dataModel::KnAllEdge map data received");
                        //Create children and parrent arrays on all nodes

                        buildInheritanceTree(knalledgeMap.knodes, knalledgeMap.kedges);
                        //Perform a topological sort
                        var topSort = topologicalSort(knalledgeMap.knodes);
                        viewModel = setViewModel(topSort, []);

                        //Fetch Query Engine
                        qeService.get()
                            .then(function(queryEngine) {

                                //Create our project collection from an array of models
                                projectCollection = queryEngine.createLiveCollection(topSort);

                                projectSearchCollection = projectCollection.createLiveChildCollection()
                                    .setPill('Name', {
                                        prefixes: ['name:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('name') === currentValue);
                                            })

                                            return result;

                                        }
                                    })
                                    .setPill('Type', {
                                        prefixes: ['Type:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('type') === currentValue);
                                            })

                                            return result;
                                        }
                                    })
                                    .setPill('Question', {
                                        prefixes: ['Question:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_ibis_question') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }
                                            else return false;
                                        }
                                    })
                                    .setPill('Knowledge', {
                                        prefixes: ['Knowledge:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_knowledge') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }

                                            else return false;
                                        }
                                    })
                                    .setPill('Idea', {
                                        prefixes: ['Idea:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_ibis_idea') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }

                                            else return false;
                                        }
                                    })
                                    .setPill('Argument', {
                                        prefixes: ['Argument:'],
                                        callback: function(model, value) {
                                            var _val = model.get('type');
                                            if (model.get('type') === 'type_ibis_argument') {
                                                var properties = splitOR(value);

                                                var result =  properties.some(function(currentValue){
                                                return (model.get('name') === currentValue);
                                                })

                                                return result;
                                            }
                                            else return false;
                                        }
                                    })
                                    .setPill('Author', {
                                        prefixes: ['Author:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('iAmId') === currentValue);
                                            })

                                            return result;
                                        }
                                    })
                                    .setPill('Created', {
                                        prefixes: ['Created:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('createdAt') === currentValue);
                                            })

                                            return result;
                                        }
                                    })
                                    .setPill('Updated', {
                                        prefixes: ['Updated:'],
                                        callback: function(model, value) {
                                            var properties = splitOR(value);

                                            var result =  properties.some(function(currentValue){
                                            return (model.get('updatedAt') === currentValue);
                                            })

                                            return result;
                                        }
                                    })

                                .setFilter('search', function(model, searchString) {
                                    if (!searchString) {
                                        return true;
                                    } else {
                                        var searchRegex = queryEngine.createSafeRegex(searchString);
                                        var passName = searchRegex.test(model.get("name"));
                                        var dataContent = model.get("dataContent");
                                        var passDescription = false;
                                        if(dataContent && dataContent.property){
                                            passDescription = searchRegex.test(dataContent.properproperty)
                                        }

                                        return passName || passDescription;
                                    }
                                });

                                var nodes = [];
                                var startIndex = 0;
                                //Done
                                dataModelReady();

                            });

                    }, function(reason) {
                        console.log("dataModel::KnAllEdge map data error");
                    });

                function splitOR(value) {
                    var properties = [];

                    if(value) {
                        properties = value.split(" or ");
                    }
                    return properties;
                }

                function splitAND(value) {
                    var properties = [];
                    if(value) {
                        properties = value.split(" and ");
                    }
                    return properties;
                }

                function buildInheritanceTree(nodes, edges) {
                    var _nodes = Array.prototype.concat(nodes, []); //Create a copy
                    for (var y = _nodes.length - 1; y >= 0; y--) {
                        _nodes[y].parents = [],
                            _nodes[y].children = [],
                            _nodes[y].parentsLinks = [],
                            _nodes[y].childrenLinks = [];
                        /* Get list property ids
                         * @param property : Name of object property to traverse
                         * @return : List of ids
                         */

                        _nodes[y].getIds = function(property) {
                            var _array = [],
                                _collection = Array.prototype.concat(this[property]);
                            for (var index in _collection) {
                                _array.push(_collection[index]._id);
                            }
                            return _array;
                        }
                    }

                    for (var i = edges.length - 1; i >= 0; i--) {
                        var sourceId = edges[i].sourceId,
                            targetId = edges[i].targetId;

                        var sourceNode = _nodes.filter(function(n) {
                                return n._id === sourceId;
                            })[0],
                            targetNode = _nodes.filter(function(n) {
                                return n._id === targetId;
                            })[0];
                        //Parent(s) are always the source of a link
                        //Child(ren) are always the target of a link
                        //Neighbors are both parents and
                        if (targetNode && sourceNode) {
                            targetNode.parents.push(sourceNode);
                            targetNode.parentsLinks.push(edges[i]);
                            sourceNode.children.push(targetNode);
                            sourceNode.childrenLinks.push(edges[i]);
                        }

                    }
                    return _nodes;
                }

                //Perform search on collection - return querynodes
                function searchCollection(searchString) {
                    var queries = searchString.split(",");
                    var queryNodes = [];
                    _.each(queries, function(searchTerm, index, list) {
                        if (/\S/.test(searchTerm) && searchTerm !== null) { //Check that search string contains at least 1 character
                            var queryNode = projectSearchCollection
                                .setSearchString(searchTerm)
                                .query()
                                .toJSON();
                            queryNodes.push({
                                "type": "queryNode",
                                "queryString": searchTerm,
                                "label": searchTerm,
                                nodes: queryNode
                            });
                        }
                    });

                    return queryNodes;
                }

                //Search for paths between query nodes - return pathnodes
                function searchQueryPaths(queryCollection, queryNodes) {
                    if (!queryNodes || queryNodes.length <= 0)
                        return [];
                    var _paths = [],
                        _startIndex = 0;
                    console.log(queryNodes);
                    //TODO: Cross check all queries. Currently only works with one result per query node.
                    //Update: Should work fine now
                    for (var i = 0; i < queryNodes.length; i++) {
                        for (var y = 0; y < queryNodes[i].nodes.length; y++) {
                            var node = queryNodes[i].nodes[y];
                            var _path = findPaths(queryCollection, queryCollection[0], node);
                            _paths.push(_path);
                        }

                    }
                    console.log("Paths found:");
                    console.log(_paths);
                    //Find all paths between two nodes
                    function findPaths(topSortGraph, qnOne, qnTwo) {
                        return shortPathDAG(topSortGraph, qnOne, qnTwo);
                    }

                    return _paths;
                }
                //Merges all paths into one graph.
                function mergePaths(paths) {
                    var _paths = Array.prototype.concat(paths, []);
                    var _graph = {},
                        _objGraph = [];

                    while (_paths.length > 0) {
                        var temp = _paths.pop();
                        for (var y = 0; y < temp.length; y++) {
                            _graph[temp[y]._id] = temp[y];
                        }
                    }
                    for (var property in _graph) {
                        _objGraph.push(_graph[property]);
                    }
                    return _objGraph;
                }

                //Top down sort nodes according to given property - returns sorted array
                function sortNodesPath(nodesArray, property) {
                    if (!nodesArray || !nodesArray.length > 0)
                        return [];
                    var sortedArray = _.sortBy(nodesArray, function(item, index, list) {
                        if (item[property])
                            return -item[property].length;
                        else return 0;
                    });
                    return sortedArray;
                }

                //Set result node size relative to paths it contain and other result nodes
                //Assumes top down sorted array - returns resized nodes array
                function setNodesSize(sortedNodeArray) {
                    if (!sortedNodeArray || !sortedNodeArray.length > 0)
                        return [];
                    var resizedNodes = sortedNodeArray;
                    //Largest possible size
                    var _maxSize = 1;
                    //Total paths on biggest result node
                    var _topNode = null;

                    _.each(resizedNodes, function(item, index, list) {
                        if (!_topNode) {
                            item.size = _maxSize;
                            _topNode = item;
                        } else {
                            item.size = item.paths.length / _topNode.paths.length;
                        }
                    });

                    return resizedNodes;
                }

                //Converge query nodes, path nodes and links and update the view model - returns view model
                function setViewModel(nodes) {
                    viewModel.nodes = nodes;
                    var links = [];

                    knalledgeMap.kedges.forEach(function(e) {
                        //http://stackoverflow.com/questions/16824308/d3-using-node-attribute-for-links-instead-of-index-in-array
                        // Get the source and target nodes
                        var sourceNode = viewModel.nodes.filter(function(n) {
                                return n._id === e.sourceId;
                            })[0],
                            targetNode = viewModel.nodes.filter(function(n) {
                                return n._id === e.targetId;
                            })[0];

                        // Add the edge to the array
                        if (sourceNode && targetNode)
                            links.push({
                                source: sourceNode,
                                target: targetNode
                            });
                    });

                    viewModel.links = links;

                    return viewModel;
                }

                //Signal that viewmodel is ready
                function dataModelReady() {
                    return $rootScope.$broadcast('dataModel::ready');
                }

                //Broadcast viewmodel updates
                function broadcastUpdate() {
                    return $rootScope.$broadcast('dataModel::viewModelUpdate');
                }

                return {
                    search: function(searchString) {
                        // console.groupCollapsed("dataModel::Search");
                        //Update viewmodel with search results
                        var _queryNodes = searchCollection(searchString);
                        var _topSort = topologicalSort(projectCollection.toJSON());
                        var _pathNodes = searchQueryPaths(_topSort, _queryNodes);
                        var _graph = mergePaths(_pathNodes);

                        // var nodes = [];
                        // for (var index in _pathNodes) {
                        //     nodes = Array.prototype.concat(_pathNodes[index].path, nodes);
                        // }

                        // var _sortedPathNodes = sortNodesPath(_pathNodes, 'paths');
                        // var _resizedNodes = setNodesSize(_sortedPathNodes);

                        // console.log("dataModel.search:: Found %i query nodes and %i path nodes.", _queryNodes.length, _pathNodes.length);

                        //Set dataModel.viewModel
                        _graph = buildInheritanceTree(_graph, knalledgeMap.kedges);
                        console.log("Graph print: ");
                        console.log(_graph);
                        setViewModel(_graph);

                        //console.log(viewModel.nodes);
                        //console.log(viewModel.links);

                        // //Tell all views that viewmodel is updated
                        broadcastUpdate();

                        // console.groupEnd();
                    },
                    getPills: function() {
                        return projectSearchCollection.getPills();
                    },
                    getFacetMatches: function(facet) {
                        var _val = '';
                        switch (facet) {
                            case 'Question':
                                _val = 'type_ibis_question';
                                break;
                            case 'Idea':
                                _val = 'type_ibis_idea';
                                break;
                            case 'Argument':
                                _val = 'type_ibis_argument';
                                break;
                            case 'Knowledge':
                                _val = 'type_knowledge';
                                break;
                              case 'Name':
                               return _.compact(_.uniq(_.flatten(projectCollection.pluck('name'))));
                                break;
                            case 'Author':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('iAmId'))));
                                break;
                            case 'Type':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('type'))));
                                break;
                            case 'Created':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('createdAt'))));
                                break;
                            case 'Updated':
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck('updatedAt'))));
                                break;
                            default:
                                return _.compact(_.uniq(_.flatten(projectCollection.pluck(facet))));
                        }
                        return _.invoke(projectCollection.where({
                            type: _val
                        }), 'get', 'name');
                    },
                    getViewModel: function() {
                        return viewModel;
                    }

                };

            }

        ]);
})();

//http://www.ng-newsletter.com/posts/d3-on-angular.html
(function() {
    angular.module('ontov')
        .service('d3Service', ['$document', '$q', '$rootScope', //D3 js
            function($document, $q, $rootScope) {
                var d = $q.defer();

                function onScriptLoad() {
                    // Load client in the browser
                    $rootScope.$apply(function() {
                        d.resolve(window.d3);
                    });
                    console.log("d3Service::D3 ready");
                }
                // Create a script tag with d3 as the source
                // and call our onScriptLoad callback when it
                // has been loaded
                var scriptTag = $document[0].createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.async = true;
                scriptTag.src = 'js/vendor/d3.min.js';
                scriptTag.charset = 'utf-8';
                scriptTag.onreadystatechange = function() {
                    if (this.readyState == 'complete') onScriptLoad();
                };
                
                scriptTag.onload = onScriptLoad;
                var s = $document[0].getElementsByTagName('body')[0];
                s.appendChild(scriptTag);
                return {
                    get: function() {

                        return d.promise;
                    }
                };
            }
        ])
        .service('vsService', ['$document', '$q', '$rootScope', //Visualsearch js
            function($document, $q, $rootScope) {
                var d = $q.defer();

                var scriptTag = $document[0].createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.async = true;
                scriptTag.src = 'js/vendor/dependencies.js';
                scriptTag.charset = 'utf-8';
                scriptTag.onreadystatechange = function() {
                    if (this.readyState == 'complete') onDependencyLoad();
                }
                scriptTag.onload = onDependencyLoad;
                var s = $document[0].getElementsByTagName('body')[0];
                s.appendChild(scriptTag);

                function onDependencyLoad() {
                    // Create a script tag with visual search as the source
                    // and call our onScriptLoad callback when it
                    // has been loaded
                    var scriptTag = $document[0].createElement('script');
                    scriptTag.type = 'text/javascript';
                    scriptTag.async = true;
                    scriptTag.src = 'js/vendor/visualsearch.js';
                    scriptTag.charset = 'utf-8';

                    scriptTag.onreadystatechange = function() {
                        if (this.readyState == 'complete') onScriptLoad();
                    }
                    scriptTag.onload = onScriptLoad;
                    var s = $document[0].getElementsByTagName('body')[0];
                    s.appendChild(scriptTag);

                    function onScriptLoad() {
                        $rootScope.$apply(function() {
                            d.resolve(window.VS);
                            console.log("vsService::Visualsearch ready");
                        });
                    }
                }

                return {
                    get: function() {

                        return d.promise;
                    }
                };
            }
        ])
        .service('qeService', ['$document', '$q', '$rootScope', 'bbService', //Queryengine js
            function($document, $q, $rootScope, bbService) {

                var d = $q.defer();
                bbService.get() //Get Backbone
                    .then(function() {
                        function onScriptLoad() {
                            // Load client in the browser
                            $rootScope.$apply(function() {
                                d.resolve(window.queryEngine);
                                console.log("qeService::Queryengine ready");
                            });
                        }
                        // Create a script tag with queryEngine as the source
                        // and call our onScriptLoad callback when it
                        // has been loaded
                        var scriptTag = $document[0].createElement('script');
                        scriptTag.type = 'text/javascript';
                        scriptTag.async = true;
                        scriptTag.src = 'js/vendor/query-engine.js';
                        scriptTag.charset = 'utf-8';
                        scriptTag.onreadystatechange = function() {
                            if (this.readyState == 'complete') onScriptLoad();
                        }
                        scriptTag.onload = onScriptLoad;
                        var s = $document[0].getElementsByTagName('body')[0];
                        s.appendChild(scriptTag);
                    })

                return {
                    get: function() {
                        return d.promise;
                    }
                };
            }
        ])
        .service('bbService', ['$document', '$q', '$rootScope', 'usService', //Backbone js
            function($document, $q, $rootScope, usService) {
                var d = $q.defer();

                usService.get() //Underscore js dependency
                    .then(function(underScore) {
                        function onScriptLoad() {
                            // Load client in the browser
                            $rootScope.$apply(function() {
                                d.resolve(window.Backbone);
                                console.log("bbService::Backbone ready");
                            });
                        }
                        // Create a script tag with Backbone as the source
                        // and call our onScriptLoad callback when it
                        // has been loaded
                        var scriptTag = $document[0].createElement('script');
                        scriptTag.type = 'text/javascript';
                        scriptTag.src = 'js/vendor/backbone-1.1.2.min.js';
                        scriptTag.charset = 'utf-8';
                        scriptTag.onreadystatechange = function() {
                            if (this.readyState == 'complete') onScriptLoad();
                        }
                        scriptTag.onload = onScriptLoad;
                        var s = $document[0].getElementsByTagName('body')[0];
                        s.appendChild(scriptTag);

                    });
                return {
                    get: function() {
                        return d.promise;
                    }
                };
            }
        ])
        .service('usService', ['$document', '$q', '$rootScope', //Underscore js
            function($document, $q, $rootScope) {
                var d = $q.defer();

                function onScriptLoad() {
                    // Load client in the browser
                    $rootScope.$apply(function() {
                        d.resolve(window.Underscore);
                        console.log("usService::Underscore ready");
                    });
                }
                // Create a script tag with Underscore as the source
                // and call our onScriptLoad callback when it
                // has been loaded
                var scriptTag = $document[0].createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.src = 'js/vendor/underscore-1.8.3.min.js';
                scriptTag.charset = 'utf-8';
                scriptTag.onreadystatechange = function() {
                    if (this.readyState == 'complete') onScriptLoad();
                }
                scriptTag.onload = onScriptLoad;
                var s = $document[0].getElementsByTagName('body')[0];
                s.appendChild(scriptTag);
                return {
                    get: function() {
                        return d.promise;
                    }
                };
            }
        ]);
        // .config(['CollaboPluginServiceProvider', function (CPServiceProvider){ //KnAllEdge plugin config
        //      //Ontov KnAllEdge specific options
        //         var ontovPluginOptions = {
        //             name: "ontovPluginOptions",
        //             private: {},
        //             references: {
        //                 map: {
        //                     items: {
        //                         config: null,
        //                         mapStructure: null
        //                     },
        //                     resolved: false,
        //                     callback: null
        //                 }
        //             },
        //             components: {},
        //             events: {}
        //         };

        //     CPServiceProvider.registerPlugin(ontovPluginOptions);
        // }])
})();
(function() {
    angular.module('ontov')
        .directive('visualSearch', ['$rootScope', '$window', 'vsService', 'dataModel', function($rootScope, $window, vsService, dataModel) {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'template/searchbar.html',
                link: function(scope, element, attrs) {

                    vsService.get()
                        .then(function(VS) {
                            var visualSearch = VS.init({
                                container: $('.visual_search'),
                                query: '',
                                callbacks: {
                                    search: function(searchString, searchCollection) {
                                        searchString = "";
                                        searchCollection.forEach(function(pill) {
                                            var category = pill.get("category"),
                                                value = pill.get("value");
                                            if (category != "text") {
                                                searchString += " " + category + ":\"" + value + "\""+",";
                                            } else {
                                                searchString += value +",";
                                            }
                                        });
                                        //Remove duplicates in search string
                                        // searchString = _.uniq(searchString.split(" "))
                                           //   .join(" ");
                                          console.log(searchString);
                                        dataModel.search(searchString);

                                    },
                                    facetMatches: function(callback) {
                                        // These are the facets that will be autocompleted in an empty input.
                                        var pills = dataModel.getPills();
                                        var pillNames = _.keys(pills);
                                        callback(pillNames);
                                    },
                                    valueMatches: function(facet, searchTerm, callback) {
                                        // These are the values that match specific categories, autocompleted
                                        // in a category's input field.  searchTerm can be used to filter the
                                        // list on the server-side, prior to providing a list to the widget.
                                        callback(dataModel.getFacetMatches(facet));
                                    }
                                }
                            });
                        });
                }
            }
        }]);

})();

angular.module('ontov')
    .directive('ontovViz', ['d3Service', '$window', 'qeService', 'dataModel', function(d3Service, $window, qeService, dataModel) {
        return {
            restrict: 'E',
            scope: {
                data: '@input',
                width: '@width',
                height: '@height'
            },
            templateUrl: 'template/visualization.html',
            link: function(scope, element, attrs) {

                //Init View Model
                var vm = {};
                var svg = element.find('svg')[0];
                svg.style.height = '1000px';
                svg.style.width = '100%';
                svg.style.overflow = 'scroll';

                //Init D3
                d3Service.get()
                    .then(function(d3) {
                        this.d3 = d3;
                    });
                scope.$on('dataModel::ready', function(event){

                    vm = dataModel.getViewModel();
                    scope.nodes = vm.nodes,
                        scope.links = vm.links;
                    drawD3Tree();
                });

                //Listen to data model changes
                scope.$on('dataModel::viewModelUpdate', function(event) {
                    vm = dataModel.getViewModel();
                    scope.nodes = vm.nodes,
                        scope.links = vm.links;
                    drawD3Tree();
                    scope.$apply();
                });

                //Update visualization
                function drawD3Tree() {
                    if(!scope.nodes || scope.nodes.length === 0){
                        return;
                    }

                    var d3 = this.d3;
                    if (d3) {
                        var tree = d3.layout.tree();
                        var clientRect = svg.getClientRects()[0];
                         tree.size([1000, clientRect.width-200]);
                            tree.separation(function separation(a, b) {
                                return a.parent == b.parent ? 50 : 50;
                            });
                            tree.nodes(scope.nodes[0]);
                    }
                }
            }
        }
    }]);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIiwiZGF0YU1vZGVsU2VydmljZS5qcyIsImRlcGVuZGVuY3lTZXJ2aWNlLmpzIiwiT250b3ZTZWFyY2hEaXJlY3RpdmUuanMiLCJPbnRvdlZpekRpcmVjdGl2ZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2xtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDak5BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3JEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im9udG92LmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9IZWxsbyBXb3JsZFxuYW5ndWxhci5tb2R1bGUoJ29udG92JywgW10pOyIsIihmdW5jdGlvbigpIHtcbiAgICAndXNlIHN0cmljdCc7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnb250b3YnKVxuICAgICAgICAuc2VydmljZSgnZGF0YU1vZGVsJywgWyckcm9vdFNjb3BlJywgJyR0aW1lb3V0JywgJ3FlU2VydmljZScsICd1c1NlcnZpY2UnLCAnJHEnLCAnJGh0dHAnLCBmdW5jdGlvbigkcm9vdFNjb3BlLCAkdGltZW91dCwgcWVTZXJ2aWNlLCB1c1NlcnZpY2UsICRxLCAkaHR0cCkge1xuICAgICAgICAgICAgICAgIC8vaHR0cDovL2pvZWxob29rcy5jb20vYmxvZy8yMDEzLzA0LzI0L21vZGVsaW5nLWRhdGEtYW5kLXN0YXRlLWluLXlvdXItYW5ndWxhcmpzLWFwcGxpY2F0aW9uL1xuXG4gICAgICAgICAgICAgICAgdmFyIHBlcmZvcm1TZWFyY2ggPSBmdW5jdGlvbigpIHt9LFxuICAgICAgICAgICAgICAgICAgICBwcm9qZWN0Q29sbGVjdGlvbiA9IHt9LFxuICAgICAgICAgICAgICAgICAgICBwcm9qZWN0U2VhcmNoQ29sbGVjdGlvbiA9IHt9O1xuXG4gICAgICAgICAgICAgICAgdmFyIHZpZXdNb2RlbCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFwibm9kZVR5cGVzXCI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInF1ZXJ5Tm9kZXNcIjogW10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwYXRoTm9kZXNcIjogW11cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBcIm5vZGVzXCI6IFtdLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJsaW5rc1wiOiBbXVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBrbmFsbGVkZ2VNYXAgPSB7fTtcblxuICAgICAgICAgICAgICAgIC8vaHR0cHM6Ly9tZ2VjaGV2LmdpdGh1Yi5pby9qYXZhc2NyaXB0LWFsZ29yaXRobXMvZ3JhcGhzX290aGVyc190b3BvbG9naWNhbC1zb3J0LmpzLmh0bWxcbiAgICAgICAgICAgICAgICB2YXIgdG9wb2xvZ2ljYWxTb3J0ID0gKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiB0b3BvbG9naWNhbFNvcnRIZWxwZXIobm9kZSwgdmlzaXRlZCwgdGVtcCwgZ3JhcGgsIHJlc3VsdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcFtub2RlXSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbmVpZ2hib3JzID0gW10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9kZUluZGV4ID0gMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBub2RlSW5zdGFuY2UgPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vR2V0IGN1cnJlbnQgbm9cbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSBncmFwaC5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChncmFwaFtpXS5faWQgPT09IG5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9kZUluc3RhbmNlID0gZ3JhcGhbaV07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5laWdoYm9ycyA9IGdyYXBoW2ldLmdldElkcygnY2hpbGRyZW4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9kZUluZGV4ID0gaTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG5laWdoYm9ycy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBuID0gbmVpZ2hib3JzW2ldO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0ZW1wW25dKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIGdyYXBoIGlzIG5vdCBhIERBRycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXZpc2l0ZWRbbl0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wb2xvZ2ljYWxTb3J0SGVscGVyKG4sIHZpc2l0ZWQsIHRlbXAsIGdyYXBoLCByZXN1bHQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBbbm9kZV0gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2l0ZWRbbm9kZV0gPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LnB1c2gobm9kZUluc3RhbmNlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgICAgICogVG9wb2xvZ2ljYWwgc29ydCBhbGdvcml0aG0gb2YgYSBkaXJlY3RlZCBhY3ljbGljIGdyYXBoLjxicj48YnI+XG4gICAgICAgICAgICAgICAgICAgICAqIFRpbWUgY29tcGxleGl0eTogTyh8RXwpIHdoZXJlIEUgaXMgYSBudW1iZXIgb2YgZWRnZXMuXG4gICAgICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oZ3JhcGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciByZXN1bHQgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB2aXNpdGVkID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGVtcCA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgbm9kZSBpbiBncmFwaCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdmlzaXRlZFtncmFwaFtub2RlXS5faWRdICYmICF0ZW1wW2dyYXBoW25vZGVdLl9pZF0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wb2xvZ2ljYWxTb3J0SGVscGVyKGdyYXBoW25vZGVdLl9pZCwgdmlzaXRlZCwgdGVtcCwgZ3JhcGgsIHJlc3VsdCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdC5yZXZlcnNlKCk7XG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSgpKTtcblxuICAgICAgICAgICAgICAgIC8vRmluZCBzaG9ydGVzdCBwYXRoIGluIHRvcG9sb2dpY2FsIG9yZGVyZWQgREFHKERpcmVjdGVkIEFjeWNsaWMgZ3JhcGgpXG4gICAgICAgICAgICAgICAgdmFyIHNob3J0UGF0aERBRyA9IChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNvc3QgPSB7fTsgLy8gQSBtYXBwaW5nIGJldHdlZW4gYSBub2RlLCB0aGUgY29zdCBvZiBpdCdzIHNob3J0ZXN0IHBhdGgsIGFuZCBpdCdzIHBhcmVudCBpbiB0aGUgc2hvcnRlc3QgcGF0aFxuICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiBmaW5kUGF0aCh0b3BTb3J0R3JhcGgsIHNvdXJjZU5vZGUsIGRlc3ROb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvL2h0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMTQ4MjYxOS9zaG9ydGVzdC1wYXRoLWZvci1hLWRhZ1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAvL0ZpbmQgdGhlIGdyYXBoIGluZGV4IG9mIHRoZSBzb3VyY2UgYW5kIGRlc3RpbmF0aW9uIG5vZGVzXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB2YXIgc3RhcnRJbmRleCA9IDAsIGVuZEluZGV4ID0gMCwgdGVtcCA9IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBmb3IgKHZhciBpID0gMDsgaSA8IHRvcFNvcnRHcmFwaC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIGlmKHNvdXJjZU5vZGUuX2lkID09PSB0b3BTb3J0R3JhcGhbaV0uX2lkKVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICBzdGFydEluZGV4ID0gaTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICBlbHNlIGlmIChkZXN0Tm9kZS5faWQgPT09IHRvcFNvcnRHcmFwaFtpXS5faWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIGVuZEluZGV4ID0gaTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vSWYgdGhlIGRlc3RpbmF0aW9uIHNvbWVob3cgaGFzIGEgbG93ZXIgaW5kZXggdGhhdCBzb3VyY2UuIFJldmVyc2UgdGhlaXIgdmFsdWVzLlxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYoc3RhcnRJbmRleDxlbmRJbmRleCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIHRlbXAgPSBzdGFydEluZGV4O1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICBzdGFydEluZGV4ID0gZW5kSW5kZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIGVuZEluZGV4ID0gc3RhcnRJbmRleDtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGZvciBlYWNoIHZlcnRleCB2IGluIHRvcF9zb3J0ZWRfbGlzdDpcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdG9wU29ydEdyYXBoLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gICBjb3N0W3ZlcnRleF0uY29zdCA9IGluZlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvc3RbdG9wU29ydEdyYXBoW2ldLl9pZF0gPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3N0W3RvcFNvcnRHcmFwaFtpXS5faWRdLmNvc3QgPSBJbmZpbml0eTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgIGNvc3RbdmVydGV4XS5wYXJlbnQgPSBOb25lXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29zdFt0b3BTb3J0R3JhcGhbaV0uX2lkXS5wYXJlbnQgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY29zdFtzb3VyY2VOb2RlLl9pZF0uY29zdCA9IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciB1ID0gMDsgdSA8IHRvcFNvcnRHcmFwaC5sZW5ndGg7IHUrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vZm9yIGVhY2ggdmVydGV4IHYgaW4gdG9wX3NvcnRlZF9saXN0OlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlZGdlcyA9IHRvcFNvcnRHcmFwaFt1XS5jaGlsZHJlbkxpbmtzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vZm9yIGVhY2ggZWRnZSBlIGluIGFkamFjZW5pZXMgb2YgdjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciB5ID0gMDsgeSA8IGVkZ2VzLmxlbmd0aDsgeSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vaWYgY29zdFtlLmRlc3RdLmNvc3QgPCBjb3N0W3ZdLmNvc3QgKyBlLndlaWdodDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHdlaWdodCA9IGVkZ2VzW3ldLndlaWdodCA/IGVkZ2VzW3ldLndlaWdodCA6IDE7IC8vSWYgZWRnZXMgZG8gbm90IGhhdmUgd2VpZ2h0cy4gU2V0IHZhbHVlIHRvIDEuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjb3N0W2VkZ2VzW3ldLnRhcmdldElkXS5jb3N0ID4gY29zdFt0b3BTb3J0R3JhcGhbdV0uX2lkXS5jb3N0ICsgd2VpZ2h0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL2Nvc3RbZS5kZXN0XS5jb3N0ID0gIGNvc3Rbdl0uY29zdCArIGUud2VpZ2h0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3N0W2VkZ2VzW3ldLnRhcmdldElkXS5jb3N0ID0gY29zdFt0b3BTb3J0R3JhcGhbdV0uX2lkXS5jb3N0ICsgd2VpZ2h0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9jb3N0W2UuZGVzdF0ucGFyZW50ID0gdlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3N0W2VkZ2VzW3ldLnRhcmdldElkXS5wYXJlbnQgPSB0b3BTb3J0R3JhcGhbdV07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24odG9wU29ydEdyYXBoLCBzb3VyY2VOb2RlLCBkZXN0Tm9kZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCF0b3BTb3J0R3JhcGggfHwgIXNvdXJjZU5vZGUgfHwgIWRlc3ROb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbnNvbGUubG9nKCdkYXRhTW9kZWw6OnNob3J0UGF0aERBRzo6XCJBcmd1bWVudHMgbWlzc2luZy4gQWJvcnRpbmcuJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZSBpZiAoc291cmNlTm9kZS5faWQgPT09IGRlc3ROb2RlLl9pZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbnNvbGUubG9nKFwiZGF0YU1vZGVsU2VydmljZTo6c2hvcnRQYXRoREFHOjpTb3VyY2UgYW5kIGRlc3RpbmF0aW9uIG5vZGUgaXMgdGhlIHNhbWUuIEFib3J0aW5nLlwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgZmluZFBhdGgodG9wU29ydEdyYXBoLCBzb3VyY2VOb2RlLCBkZXN0Tm9kZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3ViZ3JhcGggPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0YXJnZXQgPSBjb3N0W2Rlc3ROb2RlLl9pZF07XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGVtcCA9IHRhcmdldDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1YmdyYXBoLnB1c2goZGVzdE5vZGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9OQi4gT25seSBjaGVja3Mgc2luZ2xlIHBhcmVudCBub2Rlc1xuICAgICAgICAgICAgICAgICAgICAgICAgd2hpbGUgKHRlbXAucGFyZW50ICYmIHRlbXAuX2lkICE9IHNvdXJjZU5vZGUuX2lkKSB7IC8vQmFja3RyYWNrIHRhcmdldCBwYXJlbnQgY2hhaW4gdG8gZmluZCBzaG9ydGVzdCBwYXRoIHRvIHNvdXJjZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1YmdyYXBoLnB1c2godGVtcC5wYXJlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXAgPSBjb3N0W3RlbXAucGFyZW50Ll9pZF07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9JZiBzb3VyY2UgY2Fubm90IGJlIGZvdW5kLiBTb3VyY2UgaXMgbGlrZWx5IG9uIGFub3RoZXIgdHJlZSBicmFuY2guXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFzb3VyY2VOb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZGF0YU1vZGVsOjpzaG9ydFBhdGhEQUc6OiBObyBzb3VyY2Ugbm9kZSwgc291cmNlIHNldCB0byByb290LlwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3ViZ3JhcGgucmV2ZXJzZSgpO1xuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0oKSk7XG5cbiAgICAgICAgICAgICAgICAkcS5hbGwoWyRodHRwLmdldCgnZGF0YS8yMDE2LjA3LjE4LVROQy1PbmxpbmUta2VkZ2VzLmpzb24nKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBrbmFsbGVkZ2VNYXAua2VkZ2VzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCdkYXRhLzIwMTYuMDcuMTgtVE5DLU9ubGluZS1rbWFwcy5qc29uJylcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga25hbGxlZGdlTWFwLmttYXBzID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgJGh0dHAuZ2V0KCdkYXRhLzIwMTYuMDcuMTgtVE5DLU9ubGluZS1rbm9kZXMuanNvbicpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtuYWxsZWRnZU1hcC5rbm9kZXMgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImRhdGFNb2RlbDo6S25BbGxFZGdlIG1hcCBkYXRhIHJlY2VpdmVkXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9DcmVhdGUgY2hpbGRyZW4gYW5kIHBhcnJlbnQgYXJyYXlzIG9uIGFsbCBub2Rlc1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBidWlsZEluaGVyaXRhbmNlVHJlZShrbmFsbGVkZ2VNYXAua25vZGVzLCBrbmFsbGVkZ2VNYXAua2VkZ2VzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vUGVyZm9ybSBhIHRvcG9sb2dpY2FsIHNvcnRcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0b3BTb3J0ID0gdG9wb2xvZ2ljYWxTb3J0KGtuYWxsZWRnZU1hcC5rbm9kZXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmlld01vZGVsID0gc2V0Vmlld01vZGVsKHRvcFNvcnQsIFtdKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy9GZXRjaCBRdWVyeSBFbmdpbmVcbiAgICAgICAgICAgICAgICAgICAgICAgIHFlU2VydmljZS5nZXQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKHF1ZXJ5RW5naW5lKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9DcmVhdGUgb3VyIHByb2plY3QgY29sbGVjdGlvbiBmcm9tIGFuIGFycmF5IG9mIG1vZGVsc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0Q29sbGVjdGlvbiA9IHF1ZXJ5RW5naW5lLmNyZWF0ZUxpdmVDb2xsZWN0aW9uKHRvcFNvcnQpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RTZWFyY2hDb2xsZWN0aW9uID0gcHJvamVjdENvbGxlY3Rpb24uY3JlYXRlTGl2ZUNoaWxkQ29sbGVjdGlvbigpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2V0UGlsbCgnTmFtZScsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVmaXhlczogWyduYW1lOiddLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbihtb2RlbCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHByb3BlcnRpZXMgPSBzcGxpdE9SKHZhbHVlKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVzdWx0ID0gIHByb3BlcnRpZXMuc29tZShmdW5jdGlvbihjdXJyZW50VmFsdWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKG1vZGVsLmdldCgnbmFtZScpID09PSBjdXJyZW50VmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNldFBpbGwoJ1R5cGUnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlZml4ZXM6IFsnVHlwZTonXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24obW9kZWwsIHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwcm9wZXJ0aWVzID0gc3BsaXRPUih2YWx1ZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlc3VsdCA9ICBwcm9wZXJ0aWVzLnNvbWUoZnVuY3Rpb24oY3VycmVudFZhbHVlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChtb2RlbC5nZXQoJ3R5cGUnKSA9PT0gY3VycmVudFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2V0UGlsbCgnUXVlc3Rpb24nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlZml4ZXM6IFsnUXVlc3Rpb246J10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKG1vZGVsLCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgX3ZhbCA9IG1vZGVsLmdldCgndHlwZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobW9kZWwuZ2V0KCd0eXBlJykgPT09ICd0eXBlX2liaXNfcXVlc3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcHJvcGVydGllcyA9IHNwbGl0T1IodmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVzdWx0ID0gIHByb3BlcnRpZXMuc29tZShmdW5jdGlvbihjdXJyZW50VmFsdWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChtb2RlbC5nZXQoJ25hbWUnKSA9PT0gY3VycmVudFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zZXRQaWxsKCdLbm93bGVkZ2UnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlZml4ZXM6IFsnS25vd2xlZGdlOiddLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbihtb2RlbCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIF92YWwgPSBtb2RlbC5nZXQoJ3R5cGUnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1vZGVsLmdldCgndHlwZScpID09PSAndHlwZV9rbm93bGVkZ2UnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcHJvcGVydGllcyA9IHNwbGl0T1IodmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVzdWx0ID0gIHByb3BlcnRpZXMuc29tZShmdW5jdGlvbihjdXJyZW50VmFsdWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChtb2RlbC5nZXQoJ25hbWUnKSA9PT0gY3VycmVudFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNldFBpbGwoJ0lkZWEnLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlZml4ZXM6IFsnSWRlYTonXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24obW9kZWwsIHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBfdmFsID0gbW9kZWwuZ2V0KCd0eXBlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtb2RlbC5nZXQoJ3R5cGUnKSA9PT0gJ3R5cGVfaWJpc19pZGVhJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHByb3BlcnRpZXMgPSBzcGxpdE9SKHZhbHVlKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlc3VsdCA9ICBwcm9wZXJ0aWVzLnNvbWUoZnVuY3Rpb24oY3VycmVudFZhbHVlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAobW9kZWwuZ2V0KCduYW1lJykgPT09IGN1cnJlbnRWYWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zZXRQaWxsKCdBcmd1bWVudCcsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVmaXhlczogWydBcmd1bWVudDonXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24obW9kZWwsIHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBfdmFsID0gbW9kZWwuZ2V0KCd0eXBlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtb2RlbC5nZXQoJ3R5cGUnKSA9PT0gJ3R5cGVfaWJpc19hcmd1bWVudCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwcm9wZXJ0aWVzID0gc3BsaXRPUih2YWx1ZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciByZXN1bHQgPSAgcHJvcGVydGllcy5zb21lKGZ1bmN0aW9uKGN1cnJlbnRWYWx1ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKG1vZGVsLmdldCgnbmFtZScpID09PSBjdXJyZW50VmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNldFBpbGwoJ0F1dGhvcicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVmaXhlczogWydBdXRob3I6J10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKG1vZGVsLCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcHJvcGVydGllcyA9IHNwbGl0T1IodmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciByZXN1bHQgPSAgcHJvcGVydGllcy5zb21lKGZ1bmN0aW9uKGN1cnJlbnRWYWx1ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAobW9kZWwuZ2V0KCdpQW1JZCcpID09PSBjdXJyZW50VmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zZXRQaWxsKCdDcmVhdGVkJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZWZpeGVzOiBbJ0NyZWF0ZWQ6J10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uKG1vZGVsLCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcHJvcGVydGllcyA9IHNwbGl0T1IodmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciByZXN1bHQgPSAgcHJvcGVydGllcy5zb21lKGZ1bmN0aW9uKGN1cnJlbnRWYWx1ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAobW9kZWwuZ2V0KCdjcmVhdGVkQXQnKSA9PT0gY3VycmVudFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2V0UGlsbCgnVXBkYXRlZCcsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVmaXhlczogWydVcGRhdGVkOiddLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbihtb2RlbCwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHByb3BlcnRpZXMgPSBzcGxpdE9SKHZhbHVlKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcmVzdWx0ID0gIHByb3BlcnRpZXMuc29tZShmdW5jdGlvbihjdXJyZW50VmFsdWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKG1vZGVsLmdldCgndXBkYXRlZEF0JykgPT09IGN1cnJlbnRWYWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zZXRGaWx0ZXIoJ3NlYXJjaCcsIGZ1bmN0aW9uKG1vZGVsLCBzZWFyY2hTdHJpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc2VhcmNoU3RyaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzZWFyY2hSZWdleCA9IHF1ZXJ5RW5naW5lLmNyZWF0ZVNhZmVSZWdleChzZWFyY2hTdHJpbmcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwYXNzTmFtZSA9IHNlYXJjaFJlZ2V4LnRlc3QobW9kZWwuZ2V0KFwibmFtZVwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRhdGFDb250ZW50ID0gbW9kZWwuZ2V0KFwiZGF0YUNvbnRlbnRcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHBhc3NEZXNjcmlwdGlvbiA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGRhdGFDb250ZW50ICYmIGRhdGFDb250ZW50LnByb3BlcnR5KXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFzc0Rlc2NyaXB0aW9uID0gc2VhcmNoUmVnZXgudGVzdChkYXRhQ29udGVudC5wcm9wZXJwcm9wZXJ0eSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFzc05hbWUgfHzCoHBhc3NEZXNjcmlwdGlvbjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG5vZGVzID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzdGFydEluZGV4ID0gMDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9Eb25lXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFNb2RlbFJlYWR5KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbihyZWFzb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZGF0YU1vZGVsOjpLbkFsbEVkZ2UgbWFwIGRhdGEgZXJyb3JcIik7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gc3BsaXRPUih2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcHJvcGVydGllcyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0aWVzID0gdmFsdWUuc3BsaXQoXCIgb3IgXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBwcm9wZXJ0aWVzO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIHNwbGl0QU5EKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBwcm9wZXJ0aWVzID0gW107XG4gICAgICAgICAgICAgICAgICAgIGlmKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0aWVzID0gdmFsdWUuc3BsaXQoXCIgYW5kIFwiKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcHJvcGVydGllcztcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBidWlsZEluaGVyaXRhbmNlVHJlZShub2RlcywgZWRnZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIF9ub2RlcyA9IEFycmF5LnByb3RvdHlwZS5jb25jYXQobm9kZXMsIFtdKTsgLy9DcmVhdGUgYSBjb3B5XG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIHkgPSBfbm9kZXMubGVuZ3RoIC0gMTsgeSA+PSAwOyB5LS0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF9ub2Rlc1t5XS5wYXJlbnRzID0gW10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX25vZGVzW3ldLmNoaWxkcmVuID0gW10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX25vZGVzW3ldLnBhcmVudHNMaW5rcyA9IFtdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9ub2Rlc1t5XS5jaGlsZHJlbkxpbmtzID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAvKiBHZXQgbGlzdCBwcm9wZXJ0eSBpZHNcbiAgICAgICAgICAgICAgICAgICAgICAgICAqIEBwYXJhbSBwcm9wZXJ0eSA6IE5hbWUgb2Ygb2JqZWN0IHByb3BlcnR5IHRvIHRyYXZlcnNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgKiBAcmV0dXJuIDogTGlzdCBvZiBpZHNcbiAgICAgICAgICAgICAgICAgICAgICAgICAqL1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBfbm9kZXNbeV0uZ2V0SWRzID0gZnVuY3Rpb24ocHJvcGVydHkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgX2FycmF5ID0gW10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jb2xsZWN0aW9uID0gQXJyYXkucHJvdG90eXBlLmNvbmNhdCh0aGlzW3Byb3BlcnR5XSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaW5kZXggaW4gX2NvbGxlY3Rpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2FycmF5LnB1c2goX2NvbGxlY3Rpb25baW5kZXhdLl9pZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfYXJyYXk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gZWRnZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzb3VyY2VJZCA9IGVkZ2VzW2ldLnNvdXJjZUlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldElkID0gZWRnZXNbaV0udGFyZ2V0SWQ7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzb3VyY2VOb2RlID0gX25vZGVzLmZpbHRlcihmdW5jdGlvbihuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuLl9pZCA9PT0gc291cmNlSWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlbMF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0Tm9kZSA9IF9ub2Rlcy5maWx0ZXIoZnVuY3Rpb24obikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbi5faWQgPT09IHRhcmdldElkO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9QYXJlbnQocykgYXJlIGFsd2F5cyB0aGUgc291cmNlIG9mIGEgbGlua1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9DaGlsZChyZW4pIGFyZSBhbHdheXMgdGhlIHRhcmdldCBvZiBhIGxpbmtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vTmVpZ2hib3JzIGFyZSBib3RoIHBhcmVudHMgYW5kXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFyZ2V0Tm9kZSAmJiBzb3VyY2VOb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0Tm9kZS5wYXJlbnRzLnB1c2goc291cmNlTm9kZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0Tm9kZS5wYXJlbnRzTGlua3MucHVzaChlZGdlc1tpXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc291cmNlTm9kZS5jaGlsZHJlbi5wdXNoKHRhcmdldE5vZGUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNvdXJjZU5vZGUuY2hpbGRyZW5MaW5rcy5wdXNoKGVkZ2VzW2ldKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfbm9kZXM7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy9QZXJmb3JtIHNlYXJjaCBvbiBjb2xsZWN0aW9uIC0gcmV0dXJuIHF1ZXJ5bm9kZXNcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBzZWFyY2hDb2xsZWN0aW9uKHNlYXJjaFN0cmluZykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcXVlcmllcyA9IHNlYXJjaFN0cmluZy5zcGxpdChcIixcIik7XG4gICAgICAgICAgICAgICAgICAgIHZhciBxdWVyeU5vZGVzID0gW107XG4gICAgICAgICAgICAgICAgICAgIF8uZWFjaChxdWVyaWVzLCBmdW5jdGlvbihzZWFyY2hUZXJtLCBpbmRleCwgbGlzdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKC9cXFMvLnRlc3Qoc2VhcmNoVGVybSkgJiYgc2VhcmNoVGVybSAhPT0gbnVsbCkgeyAvL0NoZWNrIHRoYXQgc2VhcmNoIHN0cmluZyBjb250YWlucyBhdCBsZWFzdCAxIGNoYXJhY3RlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBxdWVyeU5vZGUgPSBwcm9qZWN0U2VhcmNoQ29sbGVjdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2V0U2VhcmNoU3RyaW5nKHNlYXJjaFRlcm0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5xdWVyeSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50b0pTT04oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWVyeU5vZGVzLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjogXCJxdWVyeU5vZGVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJxdWVyeVN0cmluZ1wiOiBzZWFyY2hUZXJtLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCI6IHNlYXJjaFRlcm0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vZGVzOiBxdWVyeU5vZGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHF1ZXJ5Tm9kZXM7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy9TZWFyY2ggZm9yIHBhdGhzIGJldHdlZW4gcXVlcnkgbm9kZXMgLSByZXR1cm4gcGF0aG5vZGVzXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gc2VhcmNoUXVlcnlQYXRocyhxdWVyeUNvbGxlY3Rpb24sIHF1ZXJ5Tm9kZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFxdWVyeU5vZGVzIHx8IHF1ZXJ5Tm9kZXMubGVuZ3RoIDw9IDApXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gW107XG4gICAgICAgICAgICAgICAgICAgIHZhciBfcGF0aHMgPSBbXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9zdGFydEluZGV4ID0gMDtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocXVlcnlOb2Rlcyk7XG4gICAgICAgICAgICAgICAgICAgIC8vVE9ETzogQ3Jvc3MgY2hlY2sgYWxsIHF1ZXJpZXMuIEN1cnJlbnRseSBvbmx5IHdvcmtzIHdpdGggb25lIHJlc3VsdCBwZXIgcXVlcnkgbm9kZS5cbiAgICAgICAgICAgICAgICAgICAgLy9VcGRhdGU6IFNob3VsZCB3b3JrIGZpbmUgbm93XG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcXVlcnlOb2Rlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgeSA9IDA7IHkgPCBxdWVyeU5vZGVzW2ldLm5vZGVzLmxlbmd0aDsgeSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG5vZGUgPSBxdWVyeU5vZGVzW2ldLm5vZGVzW3ldO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBfcGF0aCA9IGZpbmRQYXRocyhxdWVyeUNvbGxlY3Rpb24sIHF1ZXJ5Q29sbGVjdGlvblswXSwgbm9kZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3BhdGhzLnB1c2goX3BhdGgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJQYXRocyBmb3VuZDpcIik7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKF9wYXRocyk7XG4gICAgICAgICAgICAgICAgICAgIC8vRmluZCBhbGwgcGF0aHMgYmV0d2VlbiB0d28gbm9kZXNcbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gZmluZFBhdGhzKHRvcFNvcnRHcmFwaCwgcW5PbmUsIHFuVHdvKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2hvcnRQYXRoREFHKHRvcFNvcnRHcmFwaCwgcW5PbmUsIHFuVHdvKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfcGF0aHM7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vTWVyZ2VzIGFsbCBwYXRocyBpbnRvIG9uZSBncmFwaC5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBtZXJnZVBhdGhzKHBhdGhzKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBfcGF0aHMgPSBBcnJheS5wcm90b3R5cGUuY29uY2F0KHBhdGhzLCBbXSk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBfZ3JhcGggPSB7fSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9vYmpHcmFwaCA9IFtdO1xuXG4gICAgICAgICAgICAgICAgICAgIHdoaWxlIChfcGF0aHMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRlbXAgPSBfcGF0aHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciB5ID0gMDsgeSA8IHRlbXAubGVuZ3RoOyB5KyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfZ3JhcGhbdGVtcFt5XS5faWRdID0gdGVtcFt5XTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBwcm9wZXJ0eSBpbiBfZ3JhcGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIF9vYmpHcmFwaC5wdXNoKF9ncmFwaFtwcm9wZXJ0eV0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfb2JqR3JhcGg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy9Ub3AgZG93biBzb3J0IG5vZGVzIGFjY29yZGluZyB0byBnaXZlbiBwcm9wZXJ0eSAtIHJldHVybnMgc29ydGVkIGFycmF5XG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gc29ydE5vZGVzUGF0aChub2Rlc0FycmF5LCBwcm9wZXJ0eSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIW5vZGVzQXJyYXkgfHwgIW5vZGVzQXJyYXkubGVuZ3RoID4gMClcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNvcnRlZEFycmF5ID0gXy5zb3J0Qnkobm9kZXNBcnJheSwgZnVuY3Rpb24oaXRlbSwgaW5kZXgsIGxpc3QpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpdGVtW3Byb3BlcnR5XSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gLWl0ZW1bcHJvcGVydHldLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2UgcmV0dXJuIDA7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc29ydGVkQXJyYXk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy9TZXQgcmVzdWx0IG5vZGUgc2l6ZSByZWxhdGl2ZSB0byBwYXRocyBpdCBjb250YWluIGFuZCBvdGhlciByZXN1bHQgbm9kZXNcbiAgICAgICAgICAgICAgICAvL0Fzc3VtZXMgdG9wIGRvd24gc29ydGVkIGFycmF5IC0gcmV0dXJucyByZXNpemVkIG5vZGVzIGFycmF5XG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gc2V0Tm9kZXNTaXplKHNvcnRlZE5vZGVBcnJheSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIXNvcnRlZE5vZGVBcnJheSB8fCAhc29ydGVkTm9kZUFycmF5Lmxlbmd0aCA+IDApXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gW107XG4gICAgICAgICAgICAgICAgICAgIHZhciByZXNpemVkTm9kZXMgPSBzb3J0ZWROb2RlQXJyYXk7XG4gICAgICAgICAgICAgICAgICAgIC8vTGFyZ2VzdCBwb3NzaWJsZSBzaXplXG4gICAgICAgICAgICAgICAgICAgIHZhciBfbWF4U2l6ZSA9IDE7XG4gICAgICAgICAgICAgICAgICAgIC8vVG90YWwgcGF0aHMgb24gYmlnZ2VzdCByZXN1bHQgbm9kZVxuICAgICAgICAgICAgICAgICAgICB2YXIgX3RvcE5vZGUgPSBudWxsO1xuXG4gICAgICAgICAgICAgICAgICAgIF8uZWFjaChyZXNpemVkTm9kZXMsIGZ1bmN0aW9uKGl0ZW0sIGluZGV4LCBsaXN0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIV90b3BOb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbS5zaXplID0gX21heFNpemU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3RvcE5vZGUgPSBpdGVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLnNpemUgPSBpdGVtLnBhdGhzLmxlbmd0aCAvIF90b3BOb2RlLnBhdGhzLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc2l6ZWROb2RlcztcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAvL0NvbnZlcmdlIHF1ZXJ5IG5vZGVzLCBwYXRoIG5vZGVzIGFuZCBsaW5rcyBhbmQgdXBkYXRlIHRoZSB2aWV3IG1vZGVsIC0gcmV0dXJucyB2aWV3IG1vZGVsXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gc2V0Vmlld01vZGVsKG5vZGVzKSB7XG4gICAgICAgICAgICAgICAgICAgIHZpZXdNb2RlbC5ub2RlcyA9IG5vZGVzO1xuICAgICAgICAgICAgICAgICAgICB2YXIgbGlua3MgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICBrbmFsbGVkZ2VNYXAua2VkZ2VzLmZvckVhY2goZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9odHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzE2ODI0MzA4L2QzLXVzaW5nLW5vZGUtYXR0cmlidXRlLWZvci1saW5rcy1pbnN0ZWFkLW9mLWluZGV4LWluLWFycmF5XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBHZXQgdGhlIHNvdXJjZSBhbmQgdGFyZ2V0IG5vZGVzXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc291cmNlTm9kZSA9IHZpZXdNb2RlbC5ub2Rlcy5maWx0ZXIoZnVuY3Rpb24obikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbi5faWQgPT09IGUuc291cmNlSWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlbMF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0Tm9kZSA9IHZpZXdNb2RlbC5ub2Rlcy5maWx0ZXIoZnVuY3Rpb24obikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbi5faWQgPT09IGUudGFyZ2V0SWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlbMF07XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFkZCB0aGUgZWRnZSB0byB0aGUgYXJyYXlcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzb3VyY2VOb2RlICYmIHRhcmdldE5vZGUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlua3MucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNvdXJjZTogc291cmNlTm9kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OiB0YXJnZXROb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHZpZXdNb2RlbC5saW5rcyA9IGxpbmtzO1xuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB2aWV3TW9kZWw7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy9TaWduYWwgdGhhdCB2aWV3bW9kZWwgaXMgcmVhZHlcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBkYXRhTW9kZWxSZWFkeSgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRyb290U2NvcGUuJGJyb2FkY2FzdCgnZGF0YU1vZGVsOjpyZWFkeScpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vQnJvYWRjYXN0IHZpZXdtb2RlbCB1cGRhdGVzXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gYnJvYWRjYXN0VXBkYXRlKCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHJvb3RTY29wZS4kYnJvYWRjYXN0KCdkYXRhTW9kZWw6OnZpZXdNb2RlbFVwZGF0ZScpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaDogZnVuY3Rpb24oc2VhcmNoU3RyaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmdyb3VwQ29sbGFwc2VkKFwiZGF0YU1vZGVsOjpTZWFyY2hcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAvL1VwZGF0ZSB2aWV3bW9kZWwgd2l0aCBzZWFyY2ggcmVzdWx0c1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIF9xdWVyeU5vZGVzID0gc2VhcmNoQ29sbGVjdGlvbihzZWFyY2hTdHJpbmcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIF90b3BTb3J0ID0gdG9wb2xvZ2ljYWxTb3J0KHByb2plY3RDb2xsZWN0aW9uLnRvSlNPTigpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBfcGF0aE5vZGVzID0gc2VhcmNoUXVlcnlQYXRocyhfdG9wU29ydCwgX3F1ZXJ5Tm9kZXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIF9ncmFwaCA9IG1lcmdlUGF0aHMoX3BhdGhOb2Rlcyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHZhciBub2RlcyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZm9yICh2YXIgaW5kZXggaW4gX3BhdGhOb2Rlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIG5vZGVzID0gQXJyYXkucHJvdG90eXBlLmNvbmNhdChfcGF0aE5vZGVzW2luZGV4XS5wYXRoLCBub2Rlcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHZhciBfc29ydGVkUGF0aE5vZGVzID0gc29ydE5vZGVzUGF0aChfcGF0aE5vZGVzLCAncGF0aHMnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHZhciBfcmVzaXplZE5vZGVzID0gc2V0Tm9kZXNTaXplKF9zb3J0ZWRQYXRoTm9kZXMpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhcImRhdGFNb2RlbC5zZWFyY2g6OiBGb3VuZCAlaSBxdWVyeSBub2RlcyBhbmQgJWkgcGF0aCBub2Rlcy5cIiwgX3F1ZXJ5Tm9kZXMubGVuZ3RoLCBfcGF0aE5vZGVzLmxlbmd0aCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vU2V0IGRhdGFNb2RlbC52aWV3TW9kZWxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9ncmFwaCA9IGJ1aWxkSW5oZXJpdGFuY2VUcmVlKF9ncmFwaCwga25hbGxlZGdlTWFwLmtlZGdlcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkdyYXBoIHByaW50OiBcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhfZ3JhcGgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2V0Vmlld01vZGVsKF9ncmFwaCk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2codmlld01vZGVsLm5vZGVzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vY29uc29sZS5sb2codmlld01vZGVsLmxpbmtzKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gLy9UZWxsIGFsbCB2aWV3cyB0aGF0IHZpZXdtb2RlbCBpcyB1cGRhdGVkXG4gICAgICAgICAgICAgICAgICAgICAgICBicm9hZGNhc3RVcGRhdGUoKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5ncm91cEVuZCgpO1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBnZXRQaWxsczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcHJvamVjdFNlYXJjaENvbGxlY3Rpb24uZ2V0UGlsbHMoKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZ2V0RmFjZXRNYXRjaGVzOiBmdW5jdGlvbihmYWNldCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIF92YWwgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgIHN3aXRjaCAoZmFjZXQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdRdWVzdGlvbic6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92YWwgPSAndHlwZV9pYmlzX3F1ZXN0aW9uJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnSWRlYSc6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92YWwgPSAndHlwZV9pYmlzX2lkZWEnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdBcmd1bWVudCc6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92YWwgPSAndHlwZV9pYmlzX2FyZ3VtZW50JztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnS25vd2xlZGdlJzpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZhbCA9ICd0eXBlX2tub3dsZWRnZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnTmFtZSc6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF8uY29tcGFjdChfLnVuaXEoXy5mbGF0dGVuKHByb2plY3RDb2xsZWN0aW9uLnBsdWNrKCduYW1lJykpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgJ0F1dGhvcic6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfLmNvbXBhY3QoXy51bmlxKF8uZmxhdHRlbihwcm9qZWN0Q29sbGVjdGlvbi5wbHVjaygnaUFtSWQnKSkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnVHlwZSc6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfLmNvbXBhY3QoXy51bmlxKF8uZmxhdHRlbihwcm9qZWN0Q29sbGVjdGlvbi5wbHVjaygndHlwZScpKSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdDcmVhdGVkJzpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF8uY29tcGFjdChfLnVuaXEoXy5mbGF0dGVuKHByb2plY3RDb2xsZWN0aW9uLnBsdWNrKCdjcmVhdGVkQXQnKSkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnVXBkYXRlZCc6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfLmNvbXBhY3QoXy51bmlxKF8uZmxhdHRlbihwcm9qZWN0Q29sbGVjdGlvbi5wbHVjaygndXBkYXRlZEF0JykpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfLmNvbXBhY3QoXy51bmlxKF8uZmxhdHRlbihwcm9qZWN0Q29sbGVjdGlvbi5wbHVjayhmYWNldCkpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXy5pbnZva2UocHJvamVjdENvbGxlY3Rpb24ud2hlcmUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IF92YWxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLCAnZ2V0JywgJ25hbWUnKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZ2V0Vmlld01vZGVsOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB2aWV3TW9kZWw7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIH1cblxuICAgICAgICBdKTtcbn0pKCk7XG4iLCIvL2h0dHA6Ly93d3cubmctbmV3c2xldHRlci5jb20vcG9zdHMvZDMtb24tYW5ndWxhci5odG1sXG4oZnVuY3Rpb24oKSB7XG4gICAgYW5ndWxhci5tb2R1bGUoJ29udG92JylcbiAgICAgICAgLnNlcnZpY2UoJ2QzU2VydmljZScsIFsnJGRvY3VtZW50JywgJyRxJywgJyRyb290U2NvcGUnLCAvL0QzIGpzXG4gICAgICAgICAgICBmdW5jdGlvbigkZG9jdW1lbnQsICRxLCAkcm9vdFNjb3BlKSB7XG4gICAgICAgICAgICAgICAgdmFyIGQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gb25TY3JpcHRMb2FkKCkge1xuICAgICAgICAgICAgICAgICAgICAvLyBMb2FkIGNsaWVudCBpbiB0aGUgYnJvd3NlclxuICAgICAgICAgICAgICAgICAgICAkcm9vdFNjb3BlLiRhcHBseShmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGQucmVzb2x2ZSh3aW5kb3cuZDMpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJkM1NlcnZpY2U6OkQzIHJlYWR5XCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBDcmVhdGUgYSBzY3JpcHQgdGFnIHdpdGggZDMgYXMgdGhlIHNvdXJjZVxuICAgICAgICAgICAgICAgIC8vIGFuZCBjYWxsIG91ciBvblNjcmlwdExvYWQgY2FsbGJhY2sgd2hlbiBpdFxuICAgICAgICAgICAgICAgIC8vIGhhcyBiZWVuIGxvYWRlZFxuICAgICAgICAgICAgICAgIHZhciBzY3JpcHRUYWcgPSAkZG9jdW1lbnRbMF0uY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAgICAgICAgICAgc2NyaXB0VGFnLnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcbiAgICAgICAgICAgICAgICBzY3JpcHRUYWcuYXN5bmMgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5zcmMgPSAnanMvdmVuZG9yL2QzLm1pbi5qcyc7XG4gICAgICAgICAgICAgICAgc2NyaXB0VGFnLmNoYXJzZXQgPSAndXRmLTgnO1xuICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PSAnY29tcGxldGUnKSBvblNjcmlwdExvYWQoKTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5vbmxvYWQgPSBvblNjcmlwdExvYWQ7XG4gICAgICAgICAgICAgICAgdmFyIHMgPSAkZG9jdW1lbnRbMF0uZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2JvZHknKVswXTtcbiAgICAgICAgICAgICAgICBzLmFwcGVuZENoaWxkKHNjcmlwdFRhZyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGQucHJvbWlzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgIF0pXG4gICAgICAgIC5zZXJ2aWNlKCd2c1NlcnZpY2UnLCBbJyRkb2N1bWVudCcsICckcScsICckcm9vdFNjb3BlJywgLy9WaXN1YWxzZWFyY2gganNcbiAgICAgICAgICAgIGZ1bmN0aW9uKCRkb2N1bWVudCwgJHEsICRyb290U2NvcGUpIHtcbiAgICAgICAgICAgICAgICB2YXIgZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICAgICAgICAgICB2YXIgc2NyaXB0VGFnID0gJGRvY3VtZW50WzBdLmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICAgICAgICAgIHNjcmlwdFRhZy50eXBlID0gJ3RleHQvamF2YXNjcmlwdCc7XG4gICAgICAgICAgICAgICAgc2NyaXB0VGFnLmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBzY3JpcHRUYWcuc3JjID0gJ2pzL3ZlbmRvci9kZXBlbmRlbmNpZXMuanMnO1xuICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5jaGFyc2V0ID0gJ3V0Zi04JztcbiAgICAgICAgICAgICAgICBzY3JpcHRUYWcub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnJlYWR5U3RhdGUgPT0gJ2NvbXBsZXRlJykgb25EZXBlbmRlbmN5TG9hZCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzY3JpcHRUYWcub25sb2FkID0gb25EZXBlbmRlbmN5TG9hZDtcbiAgICAgICAgICAgICAgICB2YXIgcyA9ICRkb2N1bWVudFswXS5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYm9keScpWzBdO1xuICAgICAgICAgICAgICAgIHMuYXBwZW5kQ2hpbGQoc2NyaXB0VGFnKTtcblxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uIG9uRGVwZW5kZW5jeUxvYWQoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIENyZWF0ZSBhIHNjcmlwdCB0YWcgd2l0aCB2aXN1YWwgc2VhcmNoIGFzIHRoZSBzb3VyY2VcbiAgICAgICAgICAgICAgICAgICAgLy8gYW5kIGNhbGwgb3VyIG9uU2NyaXB0TG9hZCBjYWxsYmFjayB3aGVuIGl0XG4gICAgICAgICAgICAgICAgICAgIC8vIGhhcyBiZWVuIGxvYWRlZFxuICAgICAgICAgICAgICAgICAgICB2YXIgc2NyaXB0VGFnID0gJGRvY3VtZW50WzBdLmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnO1xuICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcuYXN5bmMgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcuc3JjID0gJ2pzL3ZlbmRvci92aXN1YWxzZWFyY2guanMnO1xuICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcuY2hhcnNldCA9ICd1dGYtOCc7XG5cbiAgICAgICAgICAgICAgICAgICAgc2NyaXB0VGFnLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PSAnY29tcGxldGUnKSBvblNjcmlwdExvYWQoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcub25sb2FkID0gb25TY3JpcHRMb2FkO1xuICAgICAgICAgICAgICAgICAgICB2YXIgcyA9ICRkb2N1bWVudFswXS5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYm9keScpWzBdO1xuICAgICAgICAgICAgICAgICAgICBzLmFwcGVuZENoaWxkKHNjcmlwdFRhZyk7XG5cbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gb25TY3JpcHRMb2FkKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHJvb3RTY29wZS4kYXBwbHkoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZC5yZXNvbHZlKHdpbmRvdy5WUyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJ2c1NlcnZpY2U6OlZpc3VhbHNlYXJjaCByZWFkeVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbigpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGQucHJvbWlzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgIF0pXG4gICAgICAgIC5zZXJ2aWNlKCdxZVNlcnZpY2UnLCBbJyRkb2N1bWVudCcsICckcScsICckcm9vdFNjb3BlJywgJ2JiU2VydmljZScsIC8vUXVlcnllbmdpbmUganNcbiAgICAgICAgICAgIGZ1bmN0aW9uKCRkb2N1bWVudCwgJHEsICRyb290U2NvcGUsIGJiU2VydmljZSkge1xuXG4gICAgICAgICAgICAgICAgdmFyIGQgPSAkcS5kZWZlcigpO1xuICAgICAgICAgICAgICAgIGJiU2VydmljZS5nZXQoKSAvL0dldCBCYWNrYm9uZVxuICAgICAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIG9uU2NyaXB0TG9hZCgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBMb2FkIGNsaWVudCBpbiB0aGUgYnJvd3NlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGFwcGx5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkLnJlc29sdmUod2luZG93LnF1ZXJ5RW5naW5lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJxZVNlcnZpY2U6OlF1ZXJ5ZW5naW5lIHJlYWR5XCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQ3JlYXRlIGEgc2NyaXB0IHRhZyB3aXRoIHF1ZXJ5RW5naW5lIGFzIHRoZSBzb3VyY2VcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGFuZCBjYWxsIG91ciBvblNjcmlwdExvYWQgY2FsbGJhY2sgd2hlbiBpdFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFzIGJlZW4gbG9hZGVkXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc2NyaXB0VGFnID0gJGRvY3VtZW50WzBdLmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NyaXB0VGFnLnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5hc3luYyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcuc3JjID0gJ2pzL3ZlbmRvci9xdWVyeS1lbmdpbmUuanMnO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NyaXB0VGFnLmNoYXJzZXQgPSAndXRmLTgnO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NyaXB0VGFnLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnJlYWR5U3RhdGUgPT0gJ2NvbXBsZXRlJykgb25TY3JpcHRMb2FkKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcub25sb2FkID0gb25TY3JpcHRMb2FkO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHMgPSAkZG9jdW1lbnRbMF0uZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2JvZHknKVswXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHMuYXBwZW5kQ2hpbGQoc2NyaXB0VGFnKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcblxuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZC5wcm9taXNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSlcbiAgICAgICAgLnNlcnZpY2UoJ2JiU2VydmljZScsIFsnJGRvY3VtZW50JywgJyRxJywgJyRyb290U2NvcGUnLCAndXNTZXJ2aWNlJywgLy9CYWNrYm9uZSBqc1xuICAgICAgICAgICAgZnVuY3Rpb24oJGRvY3VtZW50LCAkcSwgJHJvb3RTY29wZSwgdXNTZXJ2aWNlKSB7XG4gICAgICAgICAgICAgICAgdmFyIGQgPSAkcS5kZWZlcigpO1xuXG4gICAgICAgICAgICAgICAgdXNTZXJ2aWNlLmdldCgpIC8vVW5kZXJzY29yZSBqcyBkZXBlbmRlbmN5XG4gICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKHVuZGVyU2NvcmUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uIG9uU2NyaXB0TG9hZCgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBMb2FkIGNsaWVudCBpbiB0aGUgYnJvd3NlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGFwcGx5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkLnJlc29sdmUod2luZG93LkJhY2tib25lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJiYlNlcnZpY2U6OkJhY2tib25lIHJlYWR5XCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQ3JlYXRlIGEgc2NyaXB0IHRhZyB3aXRoIEJhY2tib25lIGFzIHRoZSBzb3VyY2VcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGFuZCBjYWxsIG91ciBvblNjcmlwdExvYWQgY2FsbGJhY2sgd2hlbiBpdFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFzIGJlZW4gbG9hZGVkXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgc2NyaXB0VGFnID0gJGRvY3VtZW50WzBdLmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NyaXB0VGFnLnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5zcmMgPSAnanMvdmVuZG9yL2JhY2tib25lLTEuMS4yLm1pbi5qcyc7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcuY2hhcnNldCA9ICd1dGYtOCc7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY3JpcHRUYWcub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PSAnY29tcGxldGUnKSBvblNjcmlwdExvYWQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5vbmxvYWQgPSBvblNjcmlwdExvYWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcyA9ICRkb2N1bWVudFswXS5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYm9keScpWzBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgcy5hcHBlbmRDaGlsZChzY3JpcHRUYWcpO1xuXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZC5wcm9taXNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSlcbiAgICAgICAgLnNlcnZpY2UoJ3VzU2VydmljZScsIFsnJGRvY3VtZW50JywgJyRxJywgJyRyb290U2NvcGUnLCAvL1VuZGVyc2NvcmUganNcbiAgICAgICAgICAgIGZ1bmN0aW9uKCRkb2N1bWVudCwgJHEsICRyb290U2NvcGUpIHtcbiAgICAgICAgICAgICAgICB2YXIgZCA9ICRxLmRlZmVyKCk7XG5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBvblNjcmlwdExvYWQoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIExvYWQgY2xpZW50IGluIHRoZSBicm93c2VyXG4gICAgICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGFwcGx5KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZC5yZXNvbHZlKHdpbmRvdy5VbmRlcnNjb3JlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidXNTZXJ2aWNlOjpVbmRlcnNjb3JlIHJlYWR5XCIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy8gQ3JlYXRlIGEgc2NyaXB0IHRhZyB3aXRoIFVuZGVyc2NvcmUgYXMgdGhlIHNvdXJjZVxuICAgICAgICAgICAgICAgIC8vIGFuZCBjYWxsIG91ciBvblNjcmlwdExvYWQgY2FsbGJhY2sgd2hlbiBpdFxuICAgICAgICAgICAgICAgIC8vIGhhcyBiZWVuIGxvYWRlZFxuICAgICAgICAgICAgICAgIHZhciBzY3JpcHRUYWcgPSAkZG9jdW1lbnRbMF0uY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAgICAgICAgICAgc2NyaXB0VGFnLnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcbiAgICAgICAgICAgICAgICBzY3JpcHRUYWcuc3JjID0gJ2pzL3ZlbmRvci91bmRlcnNjb3JlLTEuOC4zLm1pbi5qcyc7XG4gICAgICAgICAgICAgICAgc2NyaXB0VGFnLmNoYXJzZXQgPSAndXRmLTgnO1xuICAgICAgICAgICAgICAgIHNjcmlwdFRhZy5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PSAnY29tcGxldGUnKSBvblNjcmlwdExvYWQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgc2NyaXB0VGFnLm9ubG9hZCA9IG9uU2NyaXB0TG9hZDtcbiAgICAgICAgICAgICAgICB2YXIgcyA9ICRkb2N1bWVudFswXS5nZXRFbGVtZW50c0J5VGFnTmFtZSgnYm9keScpWzBdO1xuICAgICAgICAgICAgICAgIHMuYXBwZW5kQ2hpbGQoc2NyaXB0VGFnKTtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGQucHJvbWlzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgIF0pO1xuICAgICAgICAvLyAuY29uZmlnKFsnQ29sbGFib1BsdWdpblNlcnZpY2VQcm92aWRlcicsIGZ1bmN0aW9uIChDUFNlcnZpY2VQcm92aWRlcil7IC8vS25BbGxFZGdlIHBsdWdpbiBjb25maWdcbiAgICAgICAgLy8gICAgICAvL09udG92IEtuQWxsRWRnZSBzcGVjaWZpYyBvcHRpb25zXG4gICAgICAgIC8vICAgICAgICAgdmFyIG9udG92UGx1Z2luT3B0aW9ucyA9IHtcbiAgICAgICAgLy8gICAgICAgICAgICAgbmFtZTogXCJvbnRvdlBsdWdpbk9wdGlvbnNcIixcbiAgICAgICAgLy8gICAgICAgICAgICAgcHJpdmF0ZToge30sXG4gICAgICAgIC8vICAgICAgICAgICAgIHJlZmVyZW5jZXM6IHtcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIG1hcDoge1xuICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgIGl0ZW1zOiB7XG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGNvbmZpZzogbnVsbCxcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgbWFwU3RydWN0dXJlOiBudWxsXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgICAgICByZXNvbHZlZDogZmFsc2UsXG4gICAgICAgIC8vICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IG51bGxcbiAgICAgICAgLy8gICAgICAgICAgICAgICAgIH1cbiAgICAgICAgLy8gICAgICAgICAgICAgfSxcbiAgICAgICAgLy8gICAgICAgICAgICAgY29tcG9uZW50czoge30sXG4gICAgICAgIC8vICAgICAgICAgICAgIGV2ZW50czoge31cbiAgICAgICAgLy8gICAgICAgICB9O1xuXG4gICAgICAgIC8vICAgICBDUFNlcnZpY2VQcm92aWRlci5yZWdpc3RlclBsdWdpbihvbnRvdlBsdWdpbk9wdGlvbnMpO1xuICAgICAgICAvLyB9XSlcbn0pKCk7IiwiKGZ1bmN0aW9uKCkge1xuICAgIGFuZ3VsYXIubW9kdWxlKCdvbnRvdicpXG4gICAgICAgIC5kaXJlY3RpdmUoJ3Zpc3VhbFNlYXJjaCcsIFsnJHJvb3RTY29wZScsICckd2luZG93JywgJ3ZzU2VydmljZScsICdkYXRhTW9kZWwnLCBmdW5jdGlvbigkcm9vdFNjb3BlLCAkd2luZG93LCB2c1NlcnZpY2UsIGRhdGFNb2RlbCkge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICByZXN0cmljdDogJ0UnLFxuICAgICAgICAgICAgICAgIHNjb3BlOiB7fSxcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3RlbXBsYXRlL3NlYXJjaGJhci5odG1sJyxcbiAgICAgICAgICAgICAgICBsaW5rOiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cnMpIHtcblxuICAgICAgICAgICAgICAgICAgICB2c1NlcnZpY2UuZ2V0KClcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKFZTKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHZpc3VhbFNlYXJjaCA9IFZTLmluaXQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250YWluZXI6ICQoJy52aXN1YWxfc2VhcmNoJyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXJ5OiAnJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2tzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2g6IGZ1bmN0aW9uKHNlYXJjaFN0cmluZywgc2VhcmNoQ29sbGVjdGlvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaFN0cmluZyA9IFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoQ29sbGVjdGlvbi5mb3JFYWNoKGZ1bmN0aW9uKHBpbGwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNhdGVnb3J5ID0gcGlsbC5nZXQoXCJjYXRlZ29yeVwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlID0gcGlsbC5nZXQoXCJ2YWx1ZVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNhdGVnb3J5ICE9IFwidGV4dFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hTdHJpbmcgKz0gXCIgXCIgKyBjYXRlZ29yeSArIFwiOlxcXCJcIiArIHZhbHVlICsgXCJcXFwiXCIrXCIsXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hTdHJpbmcgKz0gdmFsdWUgK1wiLFwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9SZW1vdmUgZHVwbGljYXRlcyBpbiBzZWFyY2ggc3RyaW5nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gc2VhcmNoU3RyaW5nID0gXy51bmlxKHNlYXJjaFN0cmluZy5zcGxpdChcIiBcIikpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAuam9pbihcIiBcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzZWFyY2hTdHJpbmcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFNb2RlbC5zZWFyY2goc2VhcmNoU3RyaW5nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZhY2V0TWF0Y2hlczogZnVuY3Rpb24oY2FsbGJhY2spIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBUaGVzZSBhcmUgdGhlIGZhY2V0cyB0aGF0IHdpbGwgYmUgYXV0b2NvbXBsZXRlZCBpbiBhbiBlbXB0eSBpbnB1dC5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcGlsbHMgPSBkYXRhTW9kZWwuZ2V0UGlsbHMoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcGlsbE5hbWVzID0gXy5rZXlzKHBpbGxzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhwaWxsTmFtZXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlTWF0Y2hlczogZnVuY3Rpb24oZmFjZXQsIHNlYXJjaFRlcm0sIGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gVGhlc2UgYXJlIHRoZSB2YWx1ZXMgdGhhdCBtYXRjaCBzcGVjaWZpYyBjYXRlZ29yaWVzLCBhdXRvY29tcGxldGVkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gaW4gYSBjYXRlZ29yeSdzIGlucHV0IGZpZWxkLiAgc2VhcmNoVGVybSBjYW4gYmUgdXNlZCB0byBmaWx0ZXIgdGhlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbGlzdCBvbiB0aGUgc2VydmVyLXNpZGUsIHByaW9yIHRvIHByb3ZpZGluZyBhIGxpc3QgdG8gdGhlIHdpZGdldC5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhkYXRhTW9kZWwuZ2V0RmFjZXRNYXRjaGVzKGZhY2V0KSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfV0pO1xuXG59KSgpO1xuIiwiYW5ndWxhci5tb2R1bGUoJ29udG92JylcbiAgICAuZGlyZWN0aXZlKCdvbnRvdlZpeicsIFsnZDNTZXJ2aWNlJywgJyR3aW5kb3cnLCAncWVTZXJ2aWNlJywgJ2RhdGFNb2RlbCcsIGZ1bmN0aW9uKGQzU2VydmljZSwgJHdpbmRvdywgcWVTZXJ2aWNlLCBkYXRhTW9kZWwpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnRScsXG4gICAgICAgICAgICBzY29wZToge1xuICAgICAgICAgICAgICAgIGRhdGE6ICdAaW5wdXQnLFxuICAgICAgICAgICAgICAgIHdpZHRoOiAnQHdpZHRoJyxcbiAgICAgICAgICAgICAgICBoZWlnaHQ6ICdAaGVpZ2h0J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAndGVtcGxhdGUvdmlzdWFsaXphdGlvbi5odG1sJyxcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtZW50LCBhdHRycykge1xuXG4gICAgICAgICAgICAgICAgLy9Jbml0IFZpZXcgTW9kZWxcbiAgICAgICAgICAgICAgICB2YXIgdm0gPSB7fTtcbiAgICAgICAgICAgICAgICB2YXIgc3ZnID0gZWxlbWVudC5maW5kKCdzdmcnKVswXTtcbiAgICAgICAgICAgICAgICBzdmcuc3R5bGUuaGVpZ2h0ID0gJzEwMDBweCc7XG4gICAgICAgICAgICAgICAgc3ZnLnN0eWxlLndpZHRoID0gJzEwMCUnO1xuICAgICAgICAgICAgICAgIHN2Zy5zdHlsZS5vdmVyZmxvdyA9ICdzY3JvbGwnO1xuXG4gICAgICAgICAgICAgICAgLy9Jbml0IEQzXG4gICAgICAgICAgICAgICAgZDNTZXJ2aWNlLmdldCgpXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKGQzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmQzID0gZDM7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHNjb3BlLiRvbignZGF0YU1vZGVsOjpyZWFkeScsIGZ1bmN0aW9uKGV2ZW50KXtcblxuICAgICAgICAgICAgICAgICAgICB2bSA9IGRhdGFNb2RlbC5nZXRWaWV3TW9kZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUubm9kZXMgPSB2bS5ub2RlcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmxpbmtzID0gdm0ubGlua3M7XG4gICAgICAgICAgICAgICAgICAgIGRyYXdEM1RyZWUoKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIC8vTGlzdGVuIHRvIGRhdGEgbW9kZWwgY2hhbmdlc1xuICAgICAgICAgICAgICAgIHNjb3BlLiRvbignZGF0YU1vZGVsOjp2aWV3TW9kZWxVcGRhdGUnLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgICAgICAgICAgICAgICB2bSA9IGRhdGFNb2RlbC5nZXRWaWV3TW9kZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUubm9kZXMgPSB2bS5ub2RlcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmxpbmtzID0gdm0ubGlua3M7XG4gICAgICAgICAgICAgICAgICAgIGRyYXdEM1RyZWUoKTtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuJGFwcGx5KCk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAvL1VwZGF0ZSB2aXN1YWxpemF0aW9uXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gZHJhd0QzVHJlZSgpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoIXNjb3BlLm5vZGVzIHx8IHNjb3BlLm5vZGVzLmxlbmd0aCA9PT0gMCl7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB2YXIgZDMgPSB0aGlzLmQzO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZDMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0cmVlID0gZDMubGF5b3V0LnRyZWUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjbGllbnRSZWN0ID0gc3ZnLmdldENsaWVudFJlY3RzKClbMF07XG4gICAgICAgICAgICAgICAgICAgICAgICAgdHJlZS5zaXplKFsxMDAwLCBjbGllbnRSZWN0LndpZHRoLTIwMF0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyZWUuc2VwYXJhdGlvbihmdW5jdGlvbiBzZXBhcmF0aW9uKGEsIGIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGEucGFyZW50ID09IGIucGFyZW50ID8gNTAgOiA1MDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmVlLm5vZGVzKHNjb3BlLm5vZGVzWzBdKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1dKTtcbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
