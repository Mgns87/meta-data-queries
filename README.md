## Meta-data Queries Prototype

This project was initially named Ontov, short for ontology visualization. This name remains some places in the source code.

## Technology stack
- Express
- Angular
	- With Bootstrap and Angular-Bootstrap
- Node
- [D3](http://d3js.org/)
- [QueryEngine](https://github.com/bevry/query-engine)

## Get it!
NB: Make sure to have node package manager installed, this project support Node 4.x.x.

Clone this repository, and then:

```sh
$ npm install
```

Run:

```sh
$ npm start
```

Open the application by going to http://localhost:3000